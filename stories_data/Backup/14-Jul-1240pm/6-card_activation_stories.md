## Card Activation happy path 1
* greet
  - utter_greet
  - utter_welcome_amex
  - utter_help_you
* init_card_activation
  - utter_confirm_intent
  - utter_confirm_card_activate
* affirm OR confirm_card_activation_intent
  - action_card_activation_form
  - form{"name": "action_card_activation_form"}
  - form{"name": null}
  - utter_card_activate_slots_values
  - utter_confirm_proceed_transaction
* confirm_correct_info OR affirm
  - utter_transaction_confirm_message
  - utter_wait  
  - action_activate_card
  - utter_card_activation_confirm_success
  - utter_other_help
* customer_satisfied
  - utter_close_conversation
* thankyou_depart
* goodbye
  - utter_goodbye
  
## Card Activation failure path 1
* greet
  - utter_greet
  - utter_welcome_amex
  - utter_help_you
* init_card_activation
  - utter_confirm_intent
  - utter_confirm_card_activate
* affirm OR confirm_card_activation_intent
  - action_card_activation_form
  - form{"name": "action_card_activation_form"}
* out_of_scope
    - utter_ask_continue
* deny
    - action_deactivate_form
    - form{"name": null}
    - utter_goodbye
    
## Card Activation happy path 2 with exception
* greet
  - utter_greet
  - utter_welcome_amex
  - utter_help_you
* init_card_activation
  - utter_confirm_intent
  - utter_confirm_card_activate
* affirm OR confirm_card_activation_intent
  - action_card_activation_form
  - form{"name": "action_card_activation_form"}
* out_of_scope
    - utter_ask_continue
* affirm
    - action_card_activation_form
    - form{"name": null}
    - utter_card_activate_slots_values
    