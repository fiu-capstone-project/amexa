## Payments happy path 1
* greet
  - utter_greet
  - utter_welcome_amex
  - utter_help_you
* init_payment
  - utter_confirm_intent
  - utter_confirm_payment
* affirm OR confirm_payment_intent
  - payment_form
  - form{"name": "payment_form"}
  - form{"name": null}
  - utter_payment_slots_values 
  - utter_confirm_proceed_transaction 
* confirm_correct_info OR affirm
  - utter_transaction_confirm_message
  - utter_wait 
  - action_payment
  - utter_payment_confirm_success
  - utter_other_help 
* customer_satisfied OR deny
  - utter_close_conversation
* thankyou_depart OR thank OR goodbye
  - utter_goodbye
  - action_restart   
  
## Payments failure path 1
* greet
  - utter_greet
  - utter_welcome_amex
  - utter_help_you
* init_payment
  - utter_confirm_intent
  - utter_confirm_payment
* affirm OR confirm_payment_intent
  - payment_form
  - form{"name": "payment_form"}
* out_of_scope
  - utter_ask_continue
* deny
  - action_deactivate_form
  - form{"name": null}
  - utter_goodbye
  - action_restart
    
## Payments happy path 2 with exception
* greet
  - utter_greet
  - utter_welcome_amex
  - utter_help_you
* init_payment
  - utter_confirm_intent
  - utter_confirm_payment
* affirm OR confirm_payment_intent
  - payment_form
  - form{"name": "payment_form"}
* out_of_scope
  - utter_ask_continue
* affirm
  - payment_form
  - form{"name": null}
  - utter_payment_slots_values 
 