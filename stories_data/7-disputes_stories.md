
## Disputes Path 1 - User has an existing dispute. User discontinues
* greet
  - utter_greet
  - utter_welcome_amex
  - utter_help_you
* init_dispute
  - utter_confirm_intent
  - utter_confirm_dispute
* affirm OR confirm_dispute_intent
  - utter_authentication_message
  - authentication_form
  - form{"name": "authentication_form"}
  - form{"name": null}
  - action_set_authentication
  - slot{"isAuthenticated" : "True"}
  - action_check_dispute
  - slot{"hasExistingDispute" : "True"}
  - utter_existing_dispute_inform
  - utter_existing_dispute_ask_continue
* deny OR no_dispute_continue
  - utter_thanks_for_confirming
  - utter_other_help
* customer_satisfied OR deny
  - utter_close_conversation  
* thankyou_depart OR thank OR goodbye
  - utter_goodbye
  - action_restart 
  
## Disputes Path 2 - User has an existing dispute. User wants to create a new dispute case
* greet
  - utter_greet
  - utter_welcome_amex
  - utter_help_you
* init_dispute
  - utter_confirm_intent
  - utter_confirm_dispute
* affirm OR confirm_dispute_intent
  - utter_authentication_message
  - authentication_form
  - form{"name": "authentication_form"}
  - form{"name": null}
  - action_set_authentication
  - slot{"isAuthenticated" : "True"}
  - action_check_dispute
  - slot{"hasExistingDispute" : "True"}
  - utter_existing_dispute_inform
  - utter_existing_dispute_ask_continue 
* affirm OR confirm_dispute_intent
  - utter_thanks_for_confirming
  - utter_last_60days_transactions
  - action_show_transactions
  - utter_which_transaction_dispute
* dispute_case_entry
  - dispute_form
  - form{"name": "dispute_form"}
  - form{"name": null}
  - action_deDupe_slots
  - utter_dispute_slots_values 
  - utter_confirm_proceed_transaction 
* confirm_correct_info OR affirm
  - utter_transaction_confirm_message
  - utter_wait 
  - action_create_dispute_case
  - utter_dispute_confirm_success
  - utter_other_help 
* customer_satisfied OR deny
  - utter_close_conversation
* thank OR goodbye
  - utter_goodbye
  - action_restart 
  
## Disputes Path 3 - User has NO existing dispute. User wants to create a new dispute case
## interactive_Dispute_Happy Path_story_1
* greet
    - utter_greet
    - utter_welcome_amex
    - utter_help_you
* init_dispute{"case": "dispute"}
    - utter_confirm_intent
    - utter_confirm_dispute
* affirm
    - utter_authentication_message
    - authentication_form
    - form{"name": "authentication_form"}
    - slot{"requested_slot": "card4d"}
* form: card4d_entry{"card4d": "3456"}
    - slot{"card4d": "3456"}
    - form: authentication_form
    - slot{"card4d": ["3456", "3456"]}
    - slot{"requested_slot": "pin"}
* form: PIN_entry{"pin": "123456"}
    - form: authentication_form
    - slot{"pin": ["123456", "123456"]}
    - slot{"requested_slot": "name"}
* form: name_entry{"name": "Abhishek"}
    - form: authentication_form
    - slot{"name": "Abhishek"}
    - slot{"isAuthenticated": false}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_set_authentication
    - slot{"isAuthenticated": true}
    - action_check_dispute
    - slot{"hasExistingDispute": false}
    - utter_last_60days_transactions
    - action_show_transactions
    - utter_which_transaction_dispute
* dispute_case_entry{"TransID": "10011"}
    - slot{"TransID": "10011"}
    - dispute_form
    - form{"name": "dispute_form"}
    - slot{"TransID": "10011"}
    - slot{"TransID": ["10011", "10011"]}
    - slot{"requested_slot": "amount"}
* form: amount_entry{"amount": "$670"}
    - slot{"amount": "$670"}
    - form: dispute_form
    - slot{"amount": ["$670", "$670"]}
    - slot{"requested_slot": "dispReason"}
* form: dispReason_entry{"dispReason": "Duplicate Billing"}
    - form: dispute_form
    - slot{"dispReason": ["Duplicate Billing", "Duplicate Billing"]}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_deDupe_slots
    - slot{"card4d": "3456"}
    - slot{"pin": "123456"}
    - slot{"amount": "$670"}
    - slot{"dispReason": "Duplicate Billing"}
    - slot{"name": "Abhishek"}
    - slot{"TransID": "10011"}
    - utter_dispute_slots_values
    - utter_confirm_proceed_transaction
* affirm
    - utter_transaction_confirm_message
    - utter_wait
    - action_create_dispute_case
    - utter_dispute_confirm_success
    - utter_other_help
* customer_satisfied
    - utter_close_conversation
* thank
    - utter_goodbye
    - action_restart
    
## Disputes Authentication Failure 1
* greet
  - utter_greet
  - utter_welcome_amex
  - utter_help_you
* init_dispute
  - utter_confirm_intent
  - utter_confirm_dispute
* affirm OR confirm_dispute_intent
  - utter_authentication_message
  - authentication_form
  - form{"name": "authentication_form"}
  - form{"name": null}
  - action_set_authentication
  - slot{"isAuthenticated" : "False"}
  - utter_authentication_failure_close_conversation
  - utter_goodbye
  - action_restart
    