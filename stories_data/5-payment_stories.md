## Payments happy path 1
* greet
  - utter_greet
  - utter_welcome_amex
  - utter_help_you
* init_payment
  - utter_confirm_intent
  - utter_confirm_payment
* affirm OR confirm_payment_intent
  - utter_authentication_message
  - authentication_form
  - form{"name": "authentication_form"}
  - form{"name": null}
  - action_set_authentication
  - slot{"isAuthenticated" : "True"}
  - payment_form
  - form{"name": "payment_form"}
  - form{"name": null}
  - utter_payment_slots_values 
  - utter_confirm_proceed_transaction 
* confirm_correct_info OR affirm
  - utter_transaction_confirm_message
  - utter_wait 
  - action_payment
  - utter_payment_confirm_success
  - utter_other_help 
* customer_satisfied OR deny
  - utter_close_conversation
* thankyou_depart OR thank OR goodbye
  - utter_goodbye
  - action_restart   
  
## Payments failure path 1
* greet
  - utter_greet
  - utter_welcome_amex
  - utter_help_you
* init_payment
  - utter_confirm_intent
  - utter_confirm_payment
* affirm OR confirm_payment_intent
  - payment_form
  - form{"name": "payment_form"}
* out_of_scope OR user_quit
  - utter_ask_continue
* deny
  - action_deactivate_form
  - form{"name": null}
  - utter_goodbye
  - action_restart    
 
## Payment - User Authentication Failure 1
* greet
  - utter_greet
  - utter_welcome_amex
  - utter_help_you
* init_dispute
  - utter_confirm_intent
  - utter_confirm_dispute
* affirm OR confirm_payment_intent
  - utter_authentication_message
  - authentication_form
  - form{"name": "authentication_form"}
  - form{"name": null}
  - action_set_authentication
  - slot{"isAuthenticated" : "False"}
  - utter_authentication_failure_close_conversation
  - utter_goodbye
  - action_restart
 