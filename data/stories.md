## bot challenge
* bot_challenge
  - utter_iamabot

## **********       Payments happy path 1 **************

* greet
  - utter_greet
  - utter_welcome_amex
  - utter_help_you
* init_payment
  - utter_confirm_intent
  - utter_confirm_payment
* affirm OR confirm_payment_intent
  - utter_authentication_message
  - authentication_form
  - form{"name": "authentication_form"}
  - form{"name": null}
  - action_set_authentication
  - slot{"isAuthenticated" : true}
  - payment_form
  - form{"name": "payment_form"}
  - form{"name": null}
  - utter_payment_slots_values 
  - utter_confirm_proceed_transaction 
* confirm_correct_info OR affirm
  - utter_transaction_confirm_message
  - utter_wait 
  - action_payment
  - utter_payment_confirm_success
  - utter_other_help 
* customer_satisfied OR deny
  - utter_close_conversation
* thankyou_depart OR thank OR goodbye 
  - utter_goodbye
  - action_restart  
  
## Payment - User Authentication Failure 1 - Authentication Failure
* greet
  - utter_greet
  - utter_welcome_amex
  - utter_help_you
* init_dispute
  - utter_confirm_intent
  - utter_confirm_dispute
* affirm OR confirm_payment_intent
  - utter_authentication_message
  - authentication_form
  - form{"name": "authentication_form"}
  - form{"name": null}
  - action_set_authentication
  - slot{"isAuthenticated" : false}
  - utter_authentication_failure_close_conversation
  - utter_goodbye
  - action_restart
   
* greet
  - utter_greet
  - utter_welcome_amex
  - utter_help_you
* init_payment
  - utter_confirm_intent
  - utter_confirm_payment
* affirm OR confirm_payment_intent
  - utter_authentication_message
  - authentication_form
  - form{"name": "authentication_form"}
  - form{"name": null}
  - action_set_authentication
  - slot{"isAuthenticated" : true}
  - payment_form
  - form{"name": "payment_form"}
* out_of_scope OR user_quit OR deny
  - utter_ask_continue
* deny OR user_quit
  - action_deactivate_form
  - form{"name": null}
  - utter_close_conversation
  - utter_goodbye
  - action_restart
    
## Card Activation happy path 1
* greet
  - utter_greet
  - utter_welcome_amex
  - utter_help_you
* init_card_activation
  - utter_confirm_intent
  - utter_confirm_card_activate
* affirm OR confirm_card_activation_intent
  - action_card_activation_form
  - form{"name": "action_card_activation_form"}
  - form{"name": null}
  - utter_card_activate_slots_values
  - utter_confirm_proceed_transaction
* confirm_correct_info OR affirm
  - utter_transaction_confirm_message
  - utter_wait  
  - action_activate_card
  - utter_card_activation_confirm_success
  - utter_other_help
* customer_satisfied
  - utter_close_conversation
* thankyou_depart
* goodbye
  - utter_goodbye
  
## Card Activation failure path 1
* greet
  - utter_greet
  - utter_welcome_amex
  - utter_help_you
* init_card_activation
  - utter_confirm_intent
  - utter_confirm_card_activate
* affirm OR confirm_card_activation_intent
  - action_card_activation_form
  - form{"name": "action_card_activation_form"}
* out_of_scope OR user_quit
    - utter_ask_continue
* deny
    - action_deactivate_form
    - form{"name": null}
    - utter_goodbye
    
## Card Activation happy path 2 with exception
* greet
  - utter_greet
  - utter_welcome_amex
  - utter_help_you
* init_card_activation
  - utter_confirm_intent
  - utter_confirm_card_activate
* affirm OR confirm_card_activation_intent
  - action_card_activation_form
  - form{"name": "action_card_activation_form"}
* out_of_scope
    - utter_ask_continue
* affirm
    - action_card_activation_form
    - form{"name": null}
    - utter_card_activate_slots_values
    

## Disputes Path 1 - User has an existing dispute. User discontinues
* greet
  - utter_greet
  - utter_welcome_amex
  - utter_help_you
* init_dispute
  - utter_confirm_intent
  - utter_confirm_dispute
* affirm OR confirm_dispute_intent
  - utter_authentication_message
  - authentication_form
  - form{"name": "authentication_form"}
  - form{"name": null}
  - action_set_authentication
  - slot{"isAuthenticated" : true}
  - action_check_dispute
  - slot{"hasExistingDispute" : true}
  - utter_existing_dispute_inform
  - utter_existing_dispute_ask_continue
* deny OR no_dispute_continue OR user_quit
  - utter_thanks_for_confirming
  - utter_other_help
* customer_satisfied OR deny
  - utter_close_conversation  
* thankyou_depart OR thank OR goodbye
  - utter_goodbye
  - action_restart 
  
## Disputes Path 2 - User has an existing dispute. User wants to create a new dispute case
* greet
  - utter_greet
  - utter_welcome_amex
  - utter_help_you
* init_dispute
  - utter_confirm_intent
  - utter_confirm_dispute
* affirm OR confirm_dispute_intent
  - utter_authentication_message
  - authentication_form
  - form{"name": "authentication_form"}
  - form{"name": null}
  - action_set_authentication
  - slot{"isAuthenticated" : true}
  - action_check_dispute
  - slot{"hasExistingDispute" : true}
  - utter_existing_dispute_inform
  - utter_existing_dispute_ask_continue 
* affirm OR confirm_dispute_intent OR confirm_correct_info
  - utter_thanks_for_confirming
  - utter_last_60days_transactions
  - action_show_transactions
  - utter_which_transaction_dispute
* dispute_case_entry
  - dispute_form
  - form{"name": "dispute_form"}
  - form{"name": null}
  - action_deDupe_slots
  - utter_dispute_slots_values 
  - utter_confirm_proceed_transaction 
* confirm_correct_info OR affirm
  - utter_transaction_confirm_message
  - utter_wait 
  - action_create_dispute_case
  - utter_dispute_confirm_success
  - utter_other_help 
* customer_satisfied OR deny
  - utter_close_conversation
* thankyou_depart OR thank OR goodbye
  - utter_goodbye
  - action_restart   
 
## Disputes Authentication Failure 1
* greet
  - utter_greet
  - utter_welcome_amex
  - utter_help_you
* init_dispute
  - utter_confirm_intent
  - utter_confirm_dispute
* affirm OR confirm_dispute_intent
  - utter_authentication_message
  - authentication_form
  - form{"name": "authentication_form"}
  - form{"name": null}
  - action_set_authentication
  - slot{"isAuthenticated" : false}
  - utter_authentication_failure_close_conversation
  - utter_goodbye
  - action_restart
    
## Disputes Path 3 - User has NO existing dispute. User wants to create a new dispute case

* greet
    - utter_greet
    - utter_welcome_amex
    - utter_help_you
* init_dispute{"case": "dispute"}
    - utter_confirm_intent
    - utter_confirm_dispute
* affirm
    - utter_authentication_message
    - authentication_form
    - form{"name": "authentication_form"}
    - slot{"requested_slot": "card4d"}
* form: card4d_entry{"card4d": "3456"}
    - slot{"card4d": "3456"}
    - form: authentication_form
    - slot{"card4d": "3456"}
    - slot{"requested_slot": "pin"}
* form: PIN_entry{"pin": "123456"}
    - form: authentication_form
    - slot{"pin": "123456"}
    - slot{"requested_slot": "name"}
* form: name_entry{"name": "Abhishek"}
    - form: authentication_form
    - slot{"name": "Abhishek"}
    - slot{"isAuthenticated": false}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_set_authentication
    - slot{"isAuthenticated": true}
    - action_check_dispute
    - slot{"hasExistingDispute": false}
    - utter_last_60days_transactions
    - action_show_transactions
    - utter_which_transaction_dispute
* dispute_case_entry{"TransID": "10011"}
    - slot{"TransID": "10011"}
    - dispute_form
    - form{"name": "dispute_form"}
    - slot{"TransID": "10011"}
    - slot{"TransID": "10011"}
    - slot{"requested_slot": "amount"}
* form: amount_entry{"amount": "$670"}
    - slot{"amount": "$670"}
    - form: dispute_form
    - slot{"amount": "$670"}
    - slot{"requested_slot": "dispReason"}
* form: dispReason_entry{"dispReason": "Duplicate Billing"}
    - form: dispute_form
    - slot{"dispReason": "Duplicate Billing"}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_deDupe_slots
    - slot{"card4d": "3456"}
    - slot{"pin": "123456"}
    - slot{"amount": "$670"}
    - slot{"dispReason": "Duplicate Billing"}
    - slot{"name": "Abhishek"}
    - slot{"TransID": "10011"}
    - utter_dispute_slots_values
    - utter_confirm_proceed_transaction
* affirm
    - utter_transaction_confirm_message
    - utter_wait
    - action_create_dispute_case
    - utter_dispute_confirm_success
    - utter_other_help
* customer_satisfied
    - utter_close_conversation
* thank
    - utter_goodbye
    - action_restart

## interactive_story_1
* greet
    - utter_greet
    - utter_welcome_amex
    - utter_help_you
* init_dispute{"case": "dispute"}
    - utter_confirm_intent
    - utter_confirm_dispute
* affirm
    - utter_authentication_message
    - authentication_form
    - form{"name": "authentication_form"}
    - slot{"requested_slot": "card4d"}
* form: card4d_entry{"card4d": "3456"}
    - slot{"card4d": "3456"}
    - form: authentication_form
    - slot{"card4d": "3456"}
    - slot{"requested_slot": "pin"}
* form: PIN_entry{"pin": "123456"}
    - form: authentication_form
    - slot{"pin": "123456"}
    - slot{"requested_slot": "name"}
* form: name_entry{"name": "Abhishek"}
    - form: authentication_form
    - slot{"name": "Abhishek"}
    - slot{"isAuthenticated": false}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_set_authentication
    - slot{"isAuthenticated": true}
    - action_check_dispute
    - slot{"hasExistingDispute": false}
    - utter_last_60days_transactions
    - action_show_transactions
    - utter_which_transaction_dispute
* dispute_case_entry{"TransID": "10011"}
    - slot{"TransID": "10011"}
    - dispute_form
    - form{"name": "dispute_form"}
    - slot{"TransID": "10011"}
    - slot{"TransID": "10011"}
    - slot{"requested_slot": "amount"}
* form: amount_entry{"amount": "$667.00"}
    - slot{"amount": "$667.00"}
    - form: dispute_form
    - slot{"amount": "$667.00"}
    - slot{"requested_slot": "dispReason"}
* form: dispReason_entry{"dispReason": "Duplicate Billing"}
    - form: dispute_form
    - slot{"dispReason": "Duplicate Billing"}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_deDupe_slots
    - slot{"card4d": "3456"}
    - slot{"pin": "123456"}
    - slot{"amount": "$667.00"}
    - slot{"dispReason": "Duplicate Billing"}
    - slot{"name": "Abhishek"}
    - slot{"TransID": "10011"}
    - utter_dispute_slots_values
    - utter_confirm_proceed_transaction
* confirm_correct_info
    - utter_transaction_confirm_message
    - utter_wait
    - action_create_dispute_case
    - utter_dispute_confirm_success
    - utter_other_help
* customer_satisfied
    - utter_close_conversation
* goodbye
    - utter_goodbye
    - action_restart

## interactive_story_1
* greet
    - utter_greet
    - utter_welcome_amex
    - utter_help_you
* init_dispute{"case": "problem"}
    - utter_confirm_intent
    - utter_confirm_dispute
* affirm
    - utter_authentication_message
    - authentication_form
    - form{"name": "authentication_form"}
    - slot{"requested_slot": "card4d"}
* form: card4d_entry{"card4d": "1234"}
    - slot{"card4d": "1234"}
    - form: authentication_form
    - slot{"card4d": "1234"}
    - slot{"requested_slot": "pin"}
* form: PIN_entry{"pin": "234671"}
    - form: authentication_form
    - slot{"pin": "234671"}
    - slot{"requested_slot": "name"}
* form: name_entry{"name": "Priyanka"}
    - form: authentication_form
    - slot{"name": "Priyanka"}
    - slot{"isAuthenticated": false}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_set_authentication
    - slot{"isAuthenticated": false}
    - utter_authentication_failure_close_conversation
    - utter_goodbye
    - action_restart

## interactive_story_1
* greet
    - utter_greet
    - utter_welcome_amex
    - utter_help_you
* init_dispute{"case": "dispute"}
    - utter_confirm_intent
    - utter_confirm_dispute
* affirm
    - utter_authentication_message
    - authentication_form
    - form{"name": "authentication_form"}
    - slot{"requested_slot": "card4d"}
* form: card4d_entry{"card4d": "3559"}
    - slot{"card4d": "3559"}
    - form: authentication_form
    - slot{"card4d": "3559"}
    - slot{"requested_slot": "pin"}
* form: PIN_entry{"pin": "155667"}
    - form: authentication_form
    - slot{"pin": "155667"}
    - slot{"requested_slot": "name"}
* form: name_entry{"name": "Abhishek"}
    - form: authentication_form
    - slot{"name": "Abhishek"}
    - slot{"isAuthenticated": true}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_set_authentication
    - slot{"isAuthenticated": true}
    - action_check_dispute
    - slot{"hasExistingDispute": true}
    - utter_existing_dispute_inform
    - utter_existing_dispute_ask_continue
* confirm_dispute_intent
    - utter_thanks_for_confirming
    - utter_last_60days_transactions
    - action_show_transactions
    - utter_which_transaction_dispute
* dispute_case_entry{"TransID": "10011"}
    - slot{"TransID": "10011"}
    - dispute_form
    - form{"name": "dispute_form"}
    - slot{"TransID": "10011"}
    - slot{"TransID": "10011"}
    - slot{"requested_slot": "amount"}
* form: amount_entry{"amount": "$330"}
    - slot{"amount": "$330"}
    - form: dispute_form
    - slot{"amount": "$330"}
    - slot{"requested_slot": "dispReason"}
* form: dispReason_entry{"dispReason": "Duplicate Billing"}
    - form: dispute_form
    - slot{"dispReason": "Duplicate Billing"}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_deDupe_slots
    - slot{"card4d": "3559"}
    - slot{"pin": "155667"}
    - slot{"amount": "$330"}
    - slot{"dispReason": "Duplicate Billing"}
    - slot{"name": "Abhishek"}
    - slot{"TransID": "10011"}
    - utter_dispute_slots_values
    - utter_confirm_proceed_transaction
* confirm_correct_info
    - utter_transaction_confirm_message
    - utter_wait
    - action_create_dispute_case
    - utter_dispute_confirm_success
    - utter_other_help
* customer_satisfied
    - utter_close_conversation
* thank
    - utter_goodbye
    - action_restart

## interactive_story_1
* greet
    - utter_greet
    - utter_welcome_amex
    - utter_help_you
* init_dispute{"case": "incorrectly"}
    - utter_confirm_intent
    - utter_confirm_dispute
* affirm
    - utter_authentication_message
    - authentication_form
    - form{"name": "authentication_form"}
    - slot{"requested_slot": "card4d"}
* form: card4d_entry{"card4d": "3559"}
    - slot{"card4d": "3559"}
    - form: authentication_form
    - slot{"card4d": "3559"}
    - slot{"requested_slot": "pin"}
* form: PIN_entry{"pin": "155667"}
    - form: authentication_form
    - slot{"pin": "155667"}
    - slot{"requested_slot": "name"}
* form: name_entry{"name": "Alizaa"}
    - form: authentication_form
    - slot{"name": "Alizaa"}
    - slot{"isAuthenticated": true}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_set_authentication
    - slot{"isAuthenticated": true}
    - action_check_dispute
    - slot{"hasExistingDispute": true}
    - slot{"DisputeDetails": "Dispute ID: D-10042 | Date: 2020-07-10 | Merchant: California Kitchen Co. | Amount: 280.00 | Reason: Duplicate Transaction\nDispute ID: D-10011678 | Date: 2020-07-15 | Merchant: Publix Super Market | Amount: 330.00 | Reason: Duplicate Billing"}
    - utter_existing_dispute_inform
    - utter_existing_dispute_ask_continue
* no_dispute_continue OR user_quit
    - utter_thanks_for_confirming
    - utter_other_help
* customer_satisfied
    - utter_close_conversation
* thank
    - utter_goodbye
    - action_restart

## interactive_story_1
* greet
    - utter_greet
    - utter_welcome_amex
    - utter_help_you
* init_dispute{"case": "problem"}
    - utter_confirm_intent
    - utter_confirm_dispute
* affirm
    - utter_authentication_message
    - authentication_form
    - form{"name": "authentication_form"}
    - slot{"requested_slot": "card4d"}
* form: card4d_entry{"card4d": "3559"}
    - slot{"card4d": "3559"}
    - form: authentication_form
    - slot{"card4d": "3559"}
    - slot{"requested_slot": "pin"}
* form: PIN_entry{"pin": "155667"}
    - form: authentication_form
    - slot{"pin": "155667"}
    - slot{"requested_slot": "name"}
* form: name_entry{"name": "Abhishek"}
    - form: authentication_form
    - slot{"name": "Abhishek"}
    - slot{"isAuthenticated": true}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_set_authentication
    - slot{"isAuthenticated": true}
    - action_check_dispute
    - slot{"hasExistingDispute": true}
    - slot{"DisputeDetails": "Dispute ID: D-10042 | Date: 2020-07-10 | Merchant: California Kitchen Co. | Amount: 280.00 | Reason: Duplicate Transaction\nDispute ID: D-10011678 | Date: 2020-07-15 | Merchant: Publix Super Market | Amount: 330.00 | Reason: Duplicate Billing"}
    - utter_existing_dispute_inform
    - utter_existing_dispute_ask_continue
* confirm_dispute_intent
    - utter_thanks_for_confirming
    - utter_last_60days_transactions
    - action_show_transactions
    - utter_which_transaction_dispute
* dispute_case_entry{"TransID": "10041"}
    - slot{"TransID": "10041"}
    - dispute_form
    - form{"name": "dispute_form"}
    - slot{"TransID": "10041"}
    - slot{"TransID": "10041"}
    - slot{"requested_slot": "amount"}
* form: amount_entry{"amount": "$700"}
    - slot{"amount": "$700"}
    - form: dispute_form
    - slot{"amount": "$700"}
    - slot{"requested_slot": "dispReason"}
* form: dispReason_entry{"dispReason": "Goods and Services not delivered"}
    - form: dispute_form
    - slot{"dispReason": "Goods and Services not delivered"}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_deDupe_slots
    - slot{"card4d": "3559"}
    - slot{"pin": "155667"}
    - slot{"amount": "$700"}
    - slot{"dispReason": "Goods and Services not delivered"}
    - slot{"name": "Abhishek"}
    - slot{"TransID": "10041"}
    - utter_dispute_slots_values
    - utter_confirm_proceed_transaction
* confirm_correct_info
    - utter_transaction_confirm_message
    - utter_wait
    - action_create_dispute_case
    - utter_dispute_confirm_success
    - utter_other_help
* customer_satisfied
    - utter_close_conversation
* goodbye
    - utter_goodbye
    - action_restart

## interactive_story_1 - Authentication Failure Path
* greet
    - utter_greet
    - utter_welcome_amex
    - utter_help_you
* init_dispute{"case": "problem"}
    - utter_confirm_intent
    - utter_confirm_dispute
* confirm_dispute_intent
    - utter_authentication_message
    - authentication_form
    - form{"name": "authentication_form"}
    - slot{"requested_slot": "card4d"}
* form: init_dispute{"card4d": "2345"}
    - slot{"card4d": "2345"}
    - form: authentication_form
    - slot{"card4d": "2345"}
    - slot{"requested_slot": "pin"}
* form: PIN_entry{"pin": "243536"}
    - form: authentication_form
    - slot{"pin": "243536"}
    - slot{"requested_slot": "name"}
* form: name_entry{"name": "John"}
    - form: authentication_form
    - slot{"name": "John"}
    - slot{"isAuthenticated": false}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_set_authentication
    - slot{"isAuthenticated": false}
    - utter_authentication_failure_close_conversation
    - utter_goodbye
    - action_restart

## interactive_story_2
* goodbye
    - utter_goodbye

## interactive_story_1
* greet
    - utter_greet
    - utter_welcome_amex
    - utter_help_you
* init_payment
    - utter_confirm_intent
    - utter_confirm_payment
* confirm_payment_intent
    - payment_form
    - form{"name": "payment_form"}
    - slot{"requested_slot": "card4d"}
* form: card4d_entry{"card4d": "3456"}
    - slot{"card4d": "3456"}
    - form: payment_form
    - slot{"card4d": "3456"}
    - slot{"requested_slot": "payBalance"}
* form: init_payment{"payBalance": "Total"}
    - form: payment_form
    - slot{"payBalance": "Total"}
    - slot{"requested_slot": "payDate"}
* form: payment_date{"payDate": "Today"}
    - form: payment_form
    - slot{"payDate": "Today"}
    - slot{"card4d": "3"}
    - form{"name": null}
    - slot{"requested_slot": null}
    - utter_payment_slots_values
    - utter_confirm_proceed_transaction
* confirm_correct_info
    - utter_transaction_confirm_message
    - utter_wait
    - action_payment
    - utter_payment_confirm_success
    - utter_other_help
* customer_satisfied
    - utter_close_conversation
* goodbye
    - utter_goodbye
    - action_restart

## interactive_story_1 - Payments Happy Path
* greet
    - utter_greet
    - utter_welcome_amex
    - utter_help_you
* init_payment
    - utter_confirm_intent
    - utter_confirm_payment
* affirm
    - utter_authentication_message
    - authentication_form
    - form{"name": "authentication_form"}
    - slot{"requested_slot": "card4d"}
* form: card4d_entry{"card4d": "3456"}
    - slot{"card4d": "3456"}
    - form: authentication_form
    - slot{"card4d": "3456"}
    - slot{"requested_slot": "pin"}
* form: PIN_entry{"pin": "123456"}
    - form: authentication_form
    - slot{"pin": "123456"}
    - slot{"requested_slot": "name"}
* form: name_entry{"name": "Abhishek"}
    - form: authentication_form
    - slot{"name": "Abhishek"}
    - slot{"isAuthenticated": true}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_set_authentication
    - slot{"isAuthenticated": true}
    - payment_form
    - form{"name": "payment_form"}
    - slot{"requested_slot": "payBalance"}
* form: payment_amount{"payBalance": "Total"}
    - form: payment_form
    - slot{"payBalance": "Total"}
    - slot{"requested_slot": "payDate"}
* form: payment_date{"payDate": "Today"}
    - form: payment_form
    - slot{"payDate": "Today"}
    - slot{"payBalance": 1508.5}
    - slot{"payDate": "16-07-2020 23:56:53"}
    - form{"name": null}
    - slot{"requested_slot": null}
    - utter_payment_slots_values
    - utter_confirm_proceed_transaction
* confirm_correct_info
    - utter_transaction_confirm_message
    - utter_wait
    - action_payment
    - utter_payment_confirm_success
    - utter_other_help
* customer_satisfied
    - utter_close_conversation
* customer_satisfied
    - utter_goodbye
    - action_restart

## interactive_story_1 - Payments Happy Path - Custom Amount
* greet
    - utter_greet
    - utter_welcome_amex
    - utter_help_you
* init_payment
    - utter_confirm_intent
    - utter_confirm_payment
* affirm
    - utter_authentication_message
    - authentication_form
    - form{"name": "authentication_form"}
    - slot{"requested_slot": "card4d"}
* form: card4d_entry{"card4d": "3456"}
    - slot{"card4d": "3456"}
    - form: authentication_form
    - slot{"card4d": "3456"}
    - slot{"requested_slot": "pin"}
* form: PIN_entry{"pin": "123456"}
    - form: authentication_form
    - slot{"pin": "123456"}
    - slot{"requested_slot": "name"}
* form: name_entry{"name": "Abhishek"}
    - form: authentication_form
    - slot{"name": "Abhishek"}
    - slot{"isAuthenticated": true}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_set_authentication
    - slot{"isAuthenticated": true}
    - payment_form
    - form{"name": "payment_form"}
    - slot{"requested_slot": "payBalance"}
* form: payment_amount{"payBalance": "Custom Amount"}
    - form: payment_form
    - slot{"payBalance": "Custom Amount"}
    - slot{"requested_slot": "payDate"}
* form: payment_date{"payDate": "Tomorrow"}
    - form: payment_form
    - slot{"payDate": "Tomorrow"}
    - slot{"requested_slot": "amount"}
* form: amount_entry{"amount": "$330"}
    - slot{"amount": "$330"}
    - form: payment_form
    - slot{"amount": "$330"}
    - slot{"payBalance": "$330"}
    - slot{"payDate": "18-07-2020 00:53:58"}
    - form{"name": null}
    - slot{"requested_slot": null}
    - utter_payment_slots_values
    - utter_confirm_proceed_transaction
* confirm_correct_info
    - utter_transaction_confirm_message
    - utter_wait
    - action_payment
    - utter_payment_confirm_success
    - utter_other_help
* customer_satisfied
    - utter_close_conversation
* goodbye
    - utter_goodbye
    - action_restart

## interactive_story_1  - Payments Authentication Failure Scenario
* greet
    - utter_greet
    - utter_welcome_amex
    - utter_help_you
* init_dispute{"case": "dispute"}
    - utter_confirm_intent
    - utter_confirm_dispute
* affirm
    - utter_authentication_message
    - authentication_form
    - form{"name": "authentication_form"}
    - slot{"requested_slot": "card4d"}
* form: card4d_entry{"card4d": "3456"}
    - slot{"card4d": "3456"}
    - form: authentication_form
    - slot{"card4d": "3456"}
    - slot{"requested_slot": "pin"}
* form: PIN_entry{"pin": "1234556"}
    - form: authentication_form
    - slot{"pin": "1234556"}
    - slot{"requested_slot": "name"}
* form: name_entry{"name": "Dilonga Damlia"}
    - form: authentication_form
    - slot{"name": "Dilonga Damlia"}
    - slot{"isAuthenticated": false}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_set_authentication
    - slot{"isAuthenticated": false}
    - utter_authentication_failure_close_conversation
    - utter_goodbye
    - action_restart

## interactive_story_2 - Payments Happy Path
* greet
    - utter_greet
    - utter_welcome_amex
    - utter_help_you
* init_payment
    - utter_confirm_intent
    - utter_confirm_payment
* affirm
    - utter_authentication_message
    - authentication_form
    - form{"name": "authentication_form"}
    - slot{"requested_slot": "card4d"}
* form: card4d_entry{"card4d": "3456"}
    - slot{"card4d": "3456"}
    - form: authentication_form
    - slot{"card4d": "3456"}
    - slot{"requested_slot": "pin"}
* form: PIN_entry{"pin": "123456"}
    - form: authentication_form
    - slot{"pin": "123456"}
    - slot{"requested_slot": "name"}
* form: name_entry{"name": "Johnson"}
    - form: authentication_form
    - slot{"name": "Johnson"}
    - slot{"isAuthenticated": true}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_set_authentication
    - slot{"isAuthenticated": true}
    - payment_form
    - form{"name": "payment_form"}
    - slot{"payBalance": "Total"}
    - slot{"requested_slot": "payDate"}
* form: payment_date{"payDate": "Tomorrow"}
    - form: payment_form
    - slot{"payDate": "Tomorrow"}
    - slot{"payBalance": 1508.5}
    - slot{"payDate": "18-07-2020 13:18:46"}
    - form{"name": null}
    - slot{"requested_slot": null}
    - utter_payment_slots_values
    - utter_confirm_proceed_transaction
* confirm_correct_info
    - utter_transaction_confirm_message
    - utter_wait
    - action_payment
    - utter_payment_confirm_success
    - utter_other_help
* customer_satisfied
    - utter_close_conversation
* thank
    - utter_goodbye
    - action_restart

## interactive_story_1
* greet
    - utter_greet
    - utter_welcome_amex
    - utter_help_you
* init_payment
    - utter_confirm_intent
    - utter_confirm_payment
* affirm
    - utter_authentication_message
    - authentication_form
    - form{"name": "authentication_form"}
    - slot{"requested_slot": "card4d"}
* form: card4d_entry{"card4d": "3456"}
    - slot{"card4d": "3456"}
    - form: authentication_form
    - slot{"card4d": "3456"}
    - slot{"requested_slot": "pin"}
* form: PIN_entry{"pin": "123456"}
    - form: authentication_form
    - slot{"pin": "123456"}
    - slot{"requested_slot": "name"}
* form: name_entry{"name": "Tommy"}
    - form: authentication_form
    - slot{"name": "Tommy"}
    - slot{"isAuthenticated": true}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_set_authentication
    - slot{"isAuthenticated": true}
    - payment_form
    - form{"name": "payment_form"}
    - slot{"requested_slot": "payBalance"}
* form: payment_amount{"payBalance": "Total"}
    - form: payment_form
    - slot{"payBalance": "Total"}
    - slot{"requested_slot": "payDate"}
* form: payment_date{"payDate": "Tomorrow"}
    - form: payment_form
    - slot{"payDate": "Tomorrow"}
    - slot{"payBalance": 1508.5}
    - slot{"payDate": "18-07-2020 14:41:19"}
    - form{"name": null}
    - slot{"requested_slot": null}
    - utter_payment_slots_values
    - utter_confirm_proceed_transaction
* confirm_correct_info
    - utter_transaction_confirm_message
    - utter_wait
    - action_payment
    - utter_payment_confirm_success
    - utter_other_help
* customer_satisfied
    - utter_close_conversation
* goodbye
    - utter_goodbye
    - action_restart

## interactive_story_1
* greet
    - utter_greet
    - utter_welcome_amex
    - utter_help_you
* init_payment
    - utter_confirm_intent
    - utter_confirm_payment
* affirm
    - utter_authentication_message
    - authentication_form
    - form{"name": "authentication_form"}
    - slot{"requested_slot": "card4d"}
* form: card4d_entry{"card4d": "3557"}
    - slot{"card4d": "3557"}
    - form: authentication_form
    - slot{"card4d": "3557"}
    - slot{"requested_slot": "pin"}
* form: PIN_entry{"pin": "223344"}
    - form: authentication_form
    - slot{"pin": "223344"}
    - slot{"requested_slot": "name"}
* form: name_entry{"name": "Arjun"}
    - form: authentication_form
    - slot{"name": "Arjun"}
    - slot{"isAuthenticated": true}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_set_authentication
    - slot{"isAuthenticated": true}
    - payment_form
    - form{"name": "payment_form"}
    - slot{"requested_slot": "payBalance"}
* form: payment_amount{"payBalance": "Total Balance"}
    - form: payment_form
    - slot{"payBalance": "Total Balance"}
    - slot{"requested_slot": "payDate"}
* form: payment_date{"payDate": "Today"}
    - form: payment_form
    - slot{"payDate": "Today"}
    - slot{"payBalance": 1898.5}
    - slot{"payDate": "19-07-2020 10:09:46"}
    - form{"name": null}
    - slot{"requested_slot": null}
    - utter_payment_slots_values
    - utter_confirm_proceed_transaction
* confirm_correct_info
    - utter_transaction_confirm_message
    - utter_wait
    - action_payment
    - utter_payment_confirm_success
    - utter_other_help
* customer_satisfied
    - utter_close_conversation
* goodbye
    - utter_goodbye
    - action_restart

## interactive_story_1
* greet
    - utter_greet
    - utter_welcome_amex
    - utter_help_you
* init_payment{"card4d": "3456"}
    - slot{"card4d": "3456"}
    - utter_confirm_intent
    - utter_confirm_payment
* affirm
    - utter_authentication_message
    - authentication_form
    - form{"name": "authentication_form"}
    - slot{"card4d": "3456"}
    - slot{"requested_slot": "name"}
* form: name_entry{"name": "Michael"}
    - form: authentication_form
    - slot{"name": "Michael"}
    - slot{"requested_slot": "pin"}
* form: PIN_entry{"pin": "345672"}
    - slot{"pin": "345672"}
    - form: authentication_form
    - slot{"pin": "345672"}
    - slot{"isAuthenticated": false}
    - form{"name": null}
    - slot{"requested_slot": null}
    - action_set_authentication
    - slot{"isAuthenticated": false}
    - slot{"stBal": 1022.2}
    - slot{"totBal": 1508.5}
    - payment_form
    - form{"name": "payment_form"}
    - slot{"requested_slot": "payBalance"}
* form: payment_amount{"payBalance": "Statement Balance"}
    - form: payment_form
    - slot{"payBalance": "Statement Balance"}
    - slot{"requested_slot": "payDate"}
* form: payment_date{"payDate": "Today"}
    - form: payment_form
    - slot{"payDate": "Today"}
    - slot{"payBalance": 1022.2}
    - slot{"payDate": "19-07-2020 12:04:13"}
    - form{"name": null}
    - slot{"requested_slot": null}
    - utter_payment_slots_values
    - utter_confirm_proceed_transaction
* confirm_correct_info
    - utter_transaction_confirm_message
    - utter_wait
    - action_payment
    - utter_payment_confirm_success
    - utter_other_help
* customer_satisfied
    - utter_close_conversation
* goodbye
    - utter_goodbye
