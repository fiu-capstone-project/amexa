## intent:greet
- hey
- hello
- hi
- good morning
- good evening
- hey there
- Hi how are you?
- Hello Amex
- Hi Amex
- hi there
- Good Morning
- Good morning
- Good mourning
- HELLO
- HEY
- HEllo
- HI
- HI Amexa
- Hallo
- Hei
- Hellllooooooo
- Hello
- Hello Bot
- Hello amexa
- Hello Amexa
- Hello!
- Hey
- Hey Amexa
- Hey bot
- Hi
- Hi Amexa
- Hi Amexa!
- Hi bot
- Hi amexa
- Hi amexa..
- Hi there
- Hi!
- Hi'
- Hi,
- Hi, bot
- Hieee
- Hola
- Well hello there ;)
- What is up?
- Whats up
- Whats up my bot
- Whats up?
- good moring
- greet
- greetings
- hallo
- hallo amexa
- halo amexa
- heeey
- heelio
- hell9o
- hellio
- hello Amexa
- hello everybody
- hello friend
- hello hi
- hello is anybody there
- hello it is me again
- hello robot
- hello amexa
- hello, my name is [Charles Pfeffer](name)
- hello?
- helloooo
- helo
- hey bot
- hey bot!
- hey dude
- hey hey
- hey let's talk
- hey rasa
- hey amexa
- hey ther
- hey there boy
- hey there..
- hey, let's talk
- hey, Amexa!
- heya
- hi !
- hi Mister
- hi again
- hi can you speak ?
- hi folks
- hi friend
- hi friends
- hi hi
- hi i'm [Sandra](name)
- hi im [Amanda Anderson](name)
- hi mrs rasa
- hi pal!
- hi amexa
- hi there it's me
- hi!
- i am [Karen Mease](name)
- ola amexa
- rasa hello
- salut
- sup
- wasssup
- wasssup!
- what up
- whats popping
- whats up
- yo
- ssup?
- helleo
- Hello bot
- Hello How are you?
- Hi Amexa How are you ?

## intent:goodbye
- bye
- goodbye
- see you around
- see you later
- thanks Bye
- good night
- Take care
- Bye
- Bye bye
- adios
- adios?
- bye :P
- bye bot
- bye bye
- bye bye bot
- bye for now
- bye udo
- bye was nice talking to you
- bye!
- byee
- catch you later
- farewell
- good bye
- good bye rasa bot!
- goodbye.
- goodnight
- gotta go
- ok Bye
- ok bye
- ok, bye
- ok.bye
- see u later
- see ya
- see you
- see you . bye
- take care
- then bye
- tlak to you later
- toodle-oo
- Exit
- Close this talk
- byr
- adieu
- ttyl
- ttul
- talk to you later
- thank you bye
- Thank you bot, bye
- Thank you bye
- Thank you for your help. Bye

## intent:greet_depart
- have a nice day ahead
- talk to you later
- catch you later
- enjoy your weekend
- enjoy the sunday
- enjoy the saturday
- enjoy the friday
- enjoy your day
- have a nice evening
- have a great day
- enjoy rest if your day
- enoy your time
- enjoy rest if the evening
- enjoy the holidays
- have a great time ahead

## intent:thank
- Cool. Thanks
- Great, thanks
- Thank you
- Thank you Amexa
- Thank you so much
- Thank's!
- Thanks
- Thanks bot
- Thanks for that
- Thanks!
- amazing, thanks
- cheers
- cheers bro
- cool thank you
- cool thanks
- cool, thanks
- danke
- great thanks
- ok thanks
- ok thanks sara
- ok thanks!
- perfect thank you
- thank u
- thank you
- thank you anyways
- thanks
- thanks a bunch for everything
- thanks a lot
- thanks for forum link, I'll check it out
- thanks for the help
- thanks this is great news
- thanks you
- thanks!
- thankyou
- thnks
- thx
- yes thanks
- thanks for your information
- thanks f

## intent:affirm
- yes
- indeed
- of course
- that sounds good
- correct
- exactly
- thats true
- makes sense
- I am with you
- perfect
- that makes perfect sense
- Correct
- You are absolutely correct
- You bet
- Yes I agree
- You are right
- Right
- That's true
- Of course
- Yes, you are correct
- Accept
- Awesome!
- Cool
- Good
- Great
- I accept
- I accept.
- I agree
- I am using it
- I changed my mind. I want to accept it
- I do
- I get it
- I guess so
- I have used it in the past
- I will
- I'd absolutely love that
- I'm sure I will!
- I'm using it
- Nice
- OK
- Ofcourse
- Oh yes
- Oh, ok
- Ok
- Ok let's start
- Ok.
- Okay
- Okay!
- PLEASE
- SURE
- Sure
- Sweet
- That would be great
- YES
- YUP
- Yea
- Yeah
- Yeah sure
- Yep
- Yep that's fine
- Yep!
- Yepp
- Yes I do
- Yes please
- Yes please!
- Yes, I accept
- Yes.
- Yup
- a little
- absolutely
- accept
- accepted
- agreed
- agree
- ah ok
- alright
- alright, cool
- amayzing
- amazing!
- awesome
- awesome!
- confirm
- cool
- cool :)
- cool story bro
- cool!
- coolio
- definitely yes without a doubt
- done
- fair enough
- fcourse
- fine
- fuck yeah!
- go
- go ahead
- go for it
- going super well
- good.
- great
- great lets do that
- great!
- hell yeah
- hell yes
- hm, i'd like that
- how nice!
- i accept
- i agree
- i am!
- i want that
- i will!
- it is ok
- its okay
- ja
- ja cool
- ja thats great
- jezz
- jo
- k
- kk
- lets do it
- lets do this
- not bad
- ofcoure i do
- ofcourse
- oh awesome!
- oh cool
- oh good !!
- oh super
- ok
- ok cool
- ok fine
- ok friend
- ok good
- ok great
- ok i accept
- ok amexa
- ok, I behave now
- ok, I understood
- ok, Amexa
- ok...
- okay
- okay cool
- okay sure
- okay..
- oki doki
- okie
- ook
- oui
- please
- si
- sort of
- sure
- sure thing
- sure!
- that is cool
- that ok
- that sounds fine
- that's great
- thats fine
- thats good
- thats great
- very much
- well yes
- ya
- ya cool
- ya go for it
- ya i want
- ya please
- ya thats cool
- ye splease
- yea
- yeah
- yeah do that
- yeah sure
- yeah'=
- yeah, why not
- yeeeeezzzzz
- yeeees
- yep
- yep i want that
- yep if i have to
- yep please
- yep that's nice
- yep thats cool
- yep, will do thank you
- yep. :/
- yes ...
- yes I do
- yes accept please
- yes baby
- yes cool
- yes give me information
- yes go ahead
- yes go for it
- yes great
- yes i accept
- yes i agree
- yes i have built a bot before
- yes i have!
- yes it is
- yes it was okay
- yes of course
- yes pleae
- yes please
- yes please!
- yes pls
- yes sir
- yes that's great
- yes that's what i want
- yes you can
- yes'
- yes, I'd love to
- yes, cool
- yes, give me information, please
- yes,i am
- yes.
- yesh
- yess
- yessoo
- yesss
- yesssss
- yesyestyes
- yesyesyes
- yez
- yop
- you asked me a yes or no question, which i answered with yes
- you got me, I accept, if you want me to
- yres
- ys
- yup
- yyeeeh
- Okay cool
- ok..
- considering
- More a less
- cool beans
- sounds good!
- really
- Yes that's correct
- you understood me
- affirmative
- yes that's correct
- Yes that's right
- Yes.. that's correct understanding
- yes you got it :)
- Yes you are absolutely correct bot :)
- Yes that right
- Yes you got it :)

## intent:deny
- no
- never
- I don't think so
- don't like that
- no way
- not really
- that's not correct
- that's not right
- it's wrong
- wait, it's incorrect
- that's wrong
- that's incorrect
- Well no
- well no
- Well that's not correct
- I don't want to
- I don't want to give it to you
- I don't want to say
- I dont want to tell
- I'm not giving you my email address
- I'm not going to give it to you
- NEIN
- NO
- NO DON"T WANT THIS!
- Nah
- Neither
- Never
- Nevermind
- No
- No thank you
- No, not really.
- No, thank you
- No.
- Nopes
- Not really
- absolutely not
- decline
- definitely not
- deny
- i decline
- i don not like this
- i don't want either of those
- i don't want to
- i don't want to give you my email
- i dont want to
- i dont want to accept :P lol
- i guess it means - no
- i'm afraid not
- i'm not sure
- it is going pretty badly
- it sucks
- it sux
- n
- na
- nah
- nah I'm good
- nah not for me
- nah, first time
- nah, i'm good
- nehi
- nein
- neither
- never mind
- no :(
- no I dont want
- no I haven't decided yet if I want to sign up
- no and no again
- no bots at all
- no go
- no i can't
- no i don't accept
- no i dont want to
- no i dont want to accept :P lol
- no i won't
- no ma'am
- no sir
- no sorry
- no thank s
- no thank you
- no thanks
- no you did it wrong
- no!!!!
- no, i hate it
- no, my frst time
- no, thank you
- no, thanks
- no, thankyou
- no. u r idiot
- non
- noooooooooo
- noooooooooooooooooooooooooooooooooooooooo
- nop
- nope
- nope!
- nope. i am good
- not going well at all
- not right now
- not yet
- nö
- sorry not right now
- still dont want to tell
- thanks but no thanks
- this sucks
- very bad
- I do not need help installing
- I don't wanna tell the name of my company
- no stop
- stop it, i do not care!!!
- none of them
- I don't agree

## intent:customer_satisfied
- No I am good. Thank you for your support
- No I am good. Thank you
- I am good so far
- Thank you. I have nothing else
- Great! Thank you
- That's it. Thank you
- That's what I was looking for
- Cool. Thank you. I am good
- That's all I wanted
- I am satisfied
- I am good
- I am good for now. Thank you
- That's all I was looking for, thanks for your support
- Thannks for your support, that's all I wanted
- I got what I was looking for, thank you
- I am good. Thanks for your help
- No further help
- No that's all
- So far so good
- Thank you for your help and support
- This is all I was looking for
- I got my work done
- I am done
- I am done, thank you
- No I am good. Thank you for your help
- No I am good, thank you
- No I am good. Thanks for the help
- I am good so far. Thank you for your support
- No I am good
- Thank you for your support
- No I am good Thank you
- That's it, I am good for now
- I am good for now, Thanks for your help
- Well, thats it. So far so good
- No I am good so far

## intent:bot_challenge
- are you a bot?
- are you a human?
- am I talking to a bot?
- am I talking to a human?
- are you real?

## intent:customer_not_understand
- No I am good. Thank you for your support
- I am good so far
- Thank you. I have nothing else
- Great! Thank you
- That's it. Thank you
- That's what I was looking for
- Cool. Thank you. I am good

## intent:name_entry
- My name is [Abhishek](name)
- I am [Manuel](name)
- ok. I am [Michael Harrison] (name)
- My firstname is [Robin](name)
- [Adam](name)
- [Thomas Edison](name)
- myself [Nikola Tesla]
- Name [Arjun](name)
- Note my name as [Robin](name)
- [Bianca](name)
- Find my name as [Priyanka](name)
- [Abhishek](name)
- [James](name)
- My name is [Priyanka](name)
- my name is [Alizaa](name)
- my name is [John](name)
- ok..my name is [Abhishek](name)
- My name is [Abhishek](name) Kumar
- ok..my name is [Dilonga Damlia](name)
- ok..my name is [Harry](name)
- My name is [Tommy](name)
- Ok..People call me [Arjun](name)
- My name is [Michael](name)

## intent:date_entry
- date is [02/08/2020](date)
- Date [07/06/2019](date)
- Payment Date [09/12/2021](date)
- Due date [31/07/2020](date)
- The date is [26/01/2017](date)
- Keep the date as [16/02/2019](date)
- Ok. Date is [11/05/2022](date)
- ok, lets keep the date as [10/06/2019](date)
- Schedule it on [06/11/2020](date)
- schedule the payment on [09/07/1967](date)
- [11/24/2020](date)
- its [03/14/2020](date)

## intent:confirm_correct_info
- Yes these information is correct
- Yes please go ahead and make the change
- I approve the changes
- Submit the information
- Please proceed with these information
- Yes correct, please go ahead and submit
- Right information
- Please go next
- Please submit
- Yes Submit
- Submit
- I confirm, please proceed
- Yes please proceed
- I approve. Please proceed
- proceed please
- submit please
- hit submit
- approved
- submit the transaction
- yes proceed
- Please proceed
- please go ahead
- go ahead please
- go ahead
- I confirm, go ahead
- get going
- move ahead
- done, submit
- done proceed
- done, please submit
- please go next
- take action
- I confirm, take action
- I approve please take action
- I approve the details
- I confirm the details
- Go Ahead!
- yes please go ahead
- Yes please submit it
- Yes that's correct information, please proceed
- Yes the information is correct, please proceed
- Yes please go ahead
- The informations are correct, please go ahead
- All the info is correct, please proceed
- Yes these details are correct. I approve, please proceed
- Yes I approve, please go ahead

## intent:out_of_scope
- that's not what I want to do
- wait stop
- you're no help
- this is no help at all
- how old are you
- I want to order a pizza
- tell me the weather
- this isn't working
- I already told you that
- don't like that
- I don't want to tell you that
- none of your business
- that's not right
- stop asking
- nevermind
- I want to do something else
- I changed my mind

## intent:human_handoff
- Can I speak to anyone who can really help me?
- Could I talk to [Tyrone King](name)?
- I don't wanna talk to a bot
- I dont like to talk to a machine
- I want to talk to a human
- I want to talk to the founders
- are there also humans working for your company?
- can I speak to a person?
- can i please speak to a human?
- can you forward me to your team
- can you please connect me to a real rasa employee?
- can you put me in touch with a human?
- do you have human support ?
- gimme a proper human
- give me a human
- give me a human now
- human handoff
- i dont wanna talk to a bot
- i want to speak to a real person
- i want to speak to customer service
- i want to talk to a human
- i want to talk to a person
- i want to talk to human
- i want to talk to someone at rasa
- i want to talk to someone else
- i want to talk to someone who is smarter than you
- i would like to speak to a person
- i'd rather speak with a real rasa employee
- id like to talk to a real rasa employee
- let me speak with a real person please
- let me talk to a human
- let me talk to a real person
- please give me a human
- service agent
- someone from customer care
- speak to a real person
- talking to a bot is stupid
- that's annoying I'd like to speak to someone real
- thats not helping, can i talk to human?
- wrong i want to speak to a human
- can i speak to human
- can i speak to your human
- i want to chat with human
- How do I talk to a human
- talk with a human
- Can i talk to a human instead
- nevermind.... you're not human ... I need to talk to a live person
- Can you get a human to assist me?
- Can i talk to a human?
- Can I talk to a human
- Can I speak to a human?
- can i speak to a human
- no, i want to talk to human
- can you hand a conversation over to a human?
- can I talk to human?
- can I talk to human
- talk to human
- i want human :(
- can i talk to human
- i want to talk to a human \
- i want to speak to human
- can i talk to a real person?
- connect me to a real person
- I need a real person
- can i took with a real person
- let me speak to a real person
- let me speak to a real person please
- i want to talk to a real person

## intent:init_payment
- can I pay my bill?
- can I make payment?
- make payment
- I want to pay my bill
- I want to pay my last month's balance
- Let me clear my bill
- Want to pay my credit card bill
- pay my current month bill
- How do I pay my bill
- I would like to pay my 2nd month credit card bill
- Can I pay my loan payments
- I wish to pay my monthly bill
- I would like to make a [custom](payBalance) payment
- Make a payment to my green card account
- Pay my gold card bill
- I would like to pay my amex blue card bill
- what should I do to pay my bill ?
- How can I pay my amex daily card bill ?
- can I make a [custom](payBalance) payment ?
- can I schedule a payment throgh the bot ?
- Can I post date my payment ?
- Pay my [statement's](payBalance) bill
- My last [statement](payBalance) bill
- Pay my [total](payBalance) bill
- I want to close my outstanding(pay_amount) balance
- Make a [custom](payBalance) payment
- Pay [total](payBalance) bill
- Pay all [remaining](payBalance) balance
- want to pay [All](payBalance) balance on my card
- pay my [current](payBalance) balance on my card
- pay my [Total](payBalance) balance on my card
- pay my [Full](payBalance) balance on my card
- Clear my balance
- Pay my balance
- I want to make payment on my card
- I want to make payment on my card ending with [5667](card4d)
- I want to pay bill on my card [3348](card4d)
- make payment on card [7754](card4d)
- I want to make payment on card [3456](card4d)
- I want to pay [$1000](amount) on my card [3678](card4d)
- want to pay my [last](payBalance) on my card [5678](card4d)
- I want to pay [1000](amount) on my card [2567](card4d)
- My name is [Arjun](name) and my card number is [2345](card4d) and I want to make payment of $[480](amount)
- I want to pay [Total](payBalance) balance
- I need to make payment
- I want to make payment
- Well I want to make payment to my monthly bill
- Well Can you please helpe me to make a payment ?
- Well I want to make payment to my card [3456](card4d)

## intent:payment_amount
- [Total Balance](payBalance)
- [Statement Balance](payBalance)
- [Custom Amount](payBalance)
- [Total](payBalance)
- [Statement Balance](payBalance)

## intent:payment_date
- [Today](payDate)
- [Tomorrow](payDate)
- [Today](payDate)

## intent:confirm_payment_intent
- yes I want to make payment
- Yes payment
- Exactly, I want to make payment
- Yes, I want to clear my bills
- Correct, make a payment on my card
- Right, pay bill
- Yes, make payment
- That's correct, pay my bill
- That's true, pay my bill
- Right, pay my bill
- yes, make payment

## intent:init_card_activation
- can I activate my card?
- I need to activate my new card
- activate card
- get my card activated
- make my card active
- want to make my card working
- start my new card
- I am not able to use my card

## intent:PIN_entry
- Pin is [267966](pin)
- my pin is [287634](pin)
- 4 digit [098734](pin)
- pin for my card is [275327](pin)
- my card in is as follows [886289](pin)
- note down my card [298527](pin)
- new card [283727](pin)
- the pin [297689](pin)
- [123456](pin)
- My pin number is [234671](pin)
- My pin is [155667](pin)
- my pin digits are [155667](pin)
- My pin number is [155667](pin)
- My pin is [243536](pin)
- Ok..please find my pin as [5742345](pin)
- My pin is [123456](pin)
- Ok..my pin number is [123456](pin)
- Ok.my pin is [1234556](pin)
- my pin number is [123456]
- My pin number is [123456](pin)
- ok..my personal id number is [223344](pin)
- My pin number is [345672](pin)

## intent:card4d_entry
- My card number is [6678](card4d)
- card number is [2297](card4d)
- Please note my card number [9735](card4d)
- [3456](card4d)
- [1234](card4d)
- My card number is [3559](card4d)
- My last 4 digit card number is [3559](card4d)
- My card number is [3456](card4d)
- Ok.. my card number is [3456](card4d)
- Ok..so my card last 4 digits are [3456](card4d)
- My card number is ending with [3456](card4d)
- My card number ends with [3557](card4d)

## intent:confirm_card_activation_intent
- I want to activate my card
- That's right, activate my card

## intent:init_dispute
- I want to [dispute](case) my transaction
- I want to file a [dispute](case) case on my transaction
- I want to claim a [dispute](case) case
- I want to file [dispute](case) on my card
- file [dispute](case) on my card
- create a [dispute](case) case
- [dispute](case) a transaction
- I have a [problem](case) on one of the transaction
- file a [case](case) on one transaction
- merchant billed me [incorrectly](case)
- [duplicate billing](case) by merchant
- product is [defective](case)
- [incorrect](case) billing
- [dispute](case) billing
- [dispute](case) merchant
- Well I have a [problem](case) in my billing charges. I want to dispute
- I want to [dispute](case) a transaction
- Well, I want to dispute a transaction which I feel is [incorrectly](case) charged
- Well I got a [problem](case) in charges in my account
- Well I have a [problem](case) in transaction, I want to dispute a case
- I want to [dispute](case) one of my transaction

## intent:dispute_case_entry
- [first](disputeRecord) transaction
- [last](disputeRecord) transaction
- [2nd](disputeRecord) transaction
- [second](disputeRecord) transaction
- [third](disputeRecord) transaction
- [fourth](disputeRecord) transaction
- [Second](disputeRecord) transaction
- [First](disputeRecord) transaction
- [Last](disputeRecord) transaction
- transaction with ID [10011](TransID)
- Transaction with transaction id [10042](TransID)
- Transaction id [10044](TransID)
- merchant [Walmasrt](merchant)
- The merchant id [PB12345](mercahntID)
- The merchant [Publix](merchant)
- Ok let me tell the merchant id [WL23456](mercahntID)
- transaction with [$640.30](amount)
- The one with dollar [280](amount)
- the one with amount [145.50](amount)
- The one with transaction id [10054](TransID)
- Transaction with transaction id [10011](TransID)
- Ok, I want to select the one with ID [10041](TransID)

## intent:transaction_id_entry
- transaction id [10061](TransID)
- Transaction ID is [10042](TransID)
- [10052](TransID)
- ID [10051]()
- TransID: [10053](TransID)
- trans id [10061](TransID)
- id [10054](TransID)

## intent:full_partial_dispute
- [partial](disputeFraction)
- [full](disputeFraction) amount
- [partial](disputeFraction) amount
- [Full](disputeFraction) amount
- [Partial](disputeFraction) amount
- I opt for [full](disputeFraction) dispute
- I opt for [full](disputeFraction) amount
- Make it [partial](disputeFraction) amount
- make it [full](disputeFraction) amount
- Keep it [Full](disputeFraction) dispute amount
- keep it [partial](disputeFraction) dispute amount

## intent:amount_entry
- [$140.00](amount)
- $[1678.50](amount)
- amount [1245.00](amount)
- dispute amount [$450](amount)
- ok, the dispute amount would be [230.00](amount)
- [$667.00](amount)
- dispute amount [$330](amount)
- Ok, the amount will be [$700](amount)
- Amount is [$330](amount)

## intent:confirm_dispute_intent
- yes I want to dispute a transaction
- Yes disputes
- yes dispute
- dispute
- Exactly, I want to file dispute
- file dispute
- dispute a transaction
- Yes, I want to file for dispute
- Correct, file a dispute
- Right, dispute transaction
- Yes, file a Dispute
- That's correct, dispute
- That's true, I have a dispute on transaction
- Right, file a dispute
- Right, Dispute
- Right, Dispute a Transaction
- That's ok, I want to continue with a new dispute case
- Yes, please. I still want to go ahead with a new dispute case
- Yes I want to dispute a transaction

## intent:no_dispute_continue
- No I don't want to continue with this dispute process
- Stop the dispute process
- I'll not create a new dispute
- Stop this dispute
- I don't want to continue
- I don't want to create any dispute
- No dispute
- I don't want to file any dispute
- I think I am good, don't want to continue with this
- Oh ok. The dispute is already there, I wish not to continue
- I am sorry, I don't want to go further in this case
- I am sorry, I don't want to dispute anything
- I think the dispute case is there, I don't want to duplicate
- Oops sorry, I forgot. I don't want to dispute anything

## intent:user_quit
- I want to quit
- I stop
- Please stop
- Please quit the process
- I don't want to continue
- I want to stop this now
- quit
- stop
- Stop!!
- I quit
- I change my mind, I wish not to proceed
- I exit
- Exit please
- I wish not to proceed
- Lets stop here
- Lets quit
- Ok stop
- Wait, stop
- Ok quit now
- Please stop this case
- Lets stop this process

## intent:dispReason_entry
- [Duplicate billing](dispReason)
- The [Merchant billed twice](dispReason)
- dispute reason [Goods and Services not delivered](dispReason)
- [Goods and services not delivered](dispReason)
- [Defective Product](dispReason)
- [Incorrect Amount Billed](dispReason)
- [Quality issue - Product and Services](dispReason)
- The merchant did [Duplicate Billing](dispReason) on my card
- [Duplicate Billing](dispReason)
- [Goods and Services not delivered](dispReason)

## regex:TransID
- (?<!\d)(?!0000)\d{5}(?!\d)

## regex:card4d
- (?<!\d)(?!0000)\d{4}(?!\d)

## regex:pin
- (?<!\d)(?!000000)\d{6}(?!\d)
