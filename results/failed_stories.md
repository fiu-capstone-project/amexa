## happy path 1 (C:\Users\reada\AppData\Local\Temp\tmpqi7e5ahj\85a7aa79e3244989b58ae6c5d2ba93ba_conversation_tests.md)
* greet: hello there!
    - utter_greet
    - action_listen   <!-- predicted: utter_welcome_amex -->
* mood_great: amazing   <!-- predicted: affirm: amazing -->
    - utter_happy   <!-- predicted: utter_transaction_confirm_message -->


## happy path 2 (C:\Users\reada\AppData\Local\Temp\tmpqi7e5ahj\85a7aa79e3244989b58ae6c5d2ba93ba_conversation_tests.md)
* greet: hello there!
    - utter_greet
    - action_listen   <!-- predicted: utter_welcome_amex -->
* mood_great: amazing   <!-- predicted: affirm: amazing -->
    - utter_happy   <!-- predicted: utter_transaction_confirm_message -->
* goodbye: bye-bye!
    - utter_goodbye
    - action_listen   <!-- predicted: action_restart -->


## sad path 1 (C:\Users\reada\AppData\Local\Temp\tmpqi7e5ahj\85a7aa79e3244989b58ae6c5d2ba93ba_conversation_tests.md)
* greet: hello
    - utter_greet
    - action_listen   <!-- predicted: utter_welcome_amex -->
* mood_unhappy: not good   <!-- predicted: deny: not good -->
    - utter_cheer_up   <!-- predicted: utter_confirm_intent -->
    - utter_did_that_help   <!-- predicted: utter_help_you -->
* affirm: yes
    - utter_happy   <!-- predicted: utter_transaction_confirm_message -->


## sad path 2 (C:\Users\reada\AppData\Local\Temp\tmpqi7e5ahj\85a7aa79e3244989b58ae6c5d2ba93ba_conversation_tests.md)
* greet: hello
    - utter_greet
    - action_listen   <!-- predicted: utter_welcome_amex -->
* mood_unhappy: not good   <!-- predicted: deny: not good -->
    - utter_cheer_up   <!-- predicted: utter_confirm_intent -->
    - utter_did_that_help   <!-- predicted: utter_help_you -->
* deny: not really
    - utter_goodbye   <!-- predicted: utter_confirm_intent -->
    - action_listen   <!-- predicted: action_restart -->


## sad path 3 (C:\Users\reada\AppData\Local\Temp\tmpqi7e5ahj\85a7aa79e3244989b58ae6c5d2ba93ba_conversation_tests.md)
* greet: hi
    - utter_greet
    - action_listen   <!-- predicted: utter_welcome_amex -->
* mood_unhappy: very terrible   <!-- predicted: deny: very terrible -->
    - utter_cheer_up   <!-- predicted: utter_confirm_intent -->
    - utter_did_that_help   <!-- predicted: utter_help_you -->
* deny: no
    - utter_goodbye   <!-- predicted: utter_confirm_intent -->
    - action_listen   <!-- predicted: action_restart -->


