import os
import time
import sys
import colorama
import glob
import hiyapyco
from colorama import Fore, Style


# Define function to clean old files and move some of them into archive directory. Create fresh files after merging each of them.

def merge_files(path, filename, archive_path):
    combined_filename = 'combined_'+ filename + ".md"

    # Append timestamp on current file to be archived
    archived_filename = filename + "_" + time.strftime("%Y-%m-%dT%H-%M-%S") + ".md"

    print(Fore.MAGENTA + "Archiving files into archive_data folder. Cleaning other files..")
    if os.path.exists(combined_filename):
        os.remove(combined_filename)
    if os.path.exists('./data/'+ filename + '.md'):
        os.rename('./data/'+ filename + '.md', archive_path + archived_filename)
    if os.path.exists('./data/'+ filename + '.md'):
        os.remove('./data/'+ filename + '.md')

    files = os.listdir(path)
    for i in files:
        with open(path + i) as fpr:
            data = fpr.read()
        dataToWrite = data + "\n"
        with open (combined_filename, 'a+') as fpa:
            fpa.write(dataToWrite)
        fpa.close()
    fpr.close()
    if os.path.exists(combined_filename):
        if os.path.exists('./data/'+filename + '.md'):
            os.remove('./data/'+filename + '.md')
        os.rename(combined_filename, './data/'+ filename + '.md')
        print(Fore.MAGENTA + "New set of files created..")

def merge_yml_files(path, filename, archive_path):
    combined_filename = 'combined_' + filename + ".yml"

    # Append timestamp on current file to be archived
    archived_filename = filename + "_" + time.strftime("%Y-%m-%dT%H-%M-%S") + ".yml"
    print(Fore.MAGENTA + "Archiving domain.yml files into archive_data folder. Cleaning fresh file..")
    if os.path.exists(combined_filename):
        os.remove(combined_filename)

    if os.path.exists(filename + '.yml'):
        os.rename(filename + '.yml', archive_path + archived_filename)

    if os.path.exists(filename + '.yml'):
        os.remove(filename + '.yml')
    #path = 'domain_data'
    yaml_list = []
    for filename in glob.glob(os.path.join(path, '*.yml')):
        with open(filename) as fp:
            yaml_file = fp.read()
            yaml_file = yaml_file + "\n";
            yaml_list.append(yaml_file)

    merged_yaml = hiyapyco.load(yaml_list, method=hiyapyco.METHOD_MERGE)
    #print(hiyapyco.dump(merged_yaml))

    domain_yml_file = open("domain.yml", "w+")
    domain_yml_file.writelines(hiyapyco.dump(merged_yaml, default_flow_style=False))

#print("Getting command line arguments")
#print(sys.argv)

command_option = '';
for i in range (0, len(sys.argv)):
    if sys.argv[i] == 'all':
        command_option = 'all'
    if sys.argv[i] == 'n':
        command_option = 'nlu'
    if sys.argv[i] == 's':
        command_option = 'stories'
    if sys.argv[i] == 'd':
        command_option = 'domain'

if command_option == 'none':
    print("Please pass the right option for execution.  \'n\' for nlu, \'s\' for stories, \'d\' for domain and \'all\' for all")


if command_option == 'n' or command_option == 'all':
    if os.path.exists('./nlu_data'):
        nlu_files = os.listdir('./nlu_data')
        print("Getting all files from data directory....")
        #print(nlu_files)
        access_rights = 0o777
        archive_folder = './archive_data/'
        if not os.path.isdir(archive_folder):
            os.mkdir(archive_folder)

        merge_files('./nlu_data/', 'nlu', archive_folder)

if command_option == 's' or command_option == 'all':
    if os.path.exists('./stories_data'):
        stories_files = os.listdir('./stories_data')
        print(Fore.MAGENTA + "Getting all files from data directory....")
        #print(stories_files)
        access_rights = 0o777
        archive_folder = './archive_data/'
        if not os.path.isdir(archive_folder):
            os.mkdir(archive_folder)

        merge_files('./stories_data/', 'stories', archive_folder)

if command_option == 'd' or command_option == 'all':
    if os.path.exists('./domain_data'):
        domain_files = os.listdir('./domain_data')
        print(Fore.MAGENTA + "Getting all files from data directory....")
        #print(domain_files)
        access_rights = 0o777
        archive_folder = './archive_data/'
        if not os.path.isdir(archive_folder):
            os.mkdir(archive_folder)

        merge_yml_files('./domain_data/', 'domain', archive_folder)