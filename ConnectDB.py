import pyodbc
import yaml


# Function API to connect to Cloud Database. DB configurations are fetched from DBConfig.yml file
def ConnectSQLDB():
    config = {}
    with open("DBConfig.yml", "r") as stream:
        config = yaml.safe_load(stream)
    stream.close()
    server = config['server']
    database = config['database']
    username = config['username']
    password = config['password']

    print("Connecting to " + server + " " + "Database: " + database)

    # Connect to Database
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER=' + server + ';DATABASE=' + database + ';UID=' + username + ';PWD=' + password)
    cursor = cnxn.cursor()
    return cnxn, cursor   # Return the connection and cursor object



#ConnectSQLDB()