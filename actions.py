# This files contains your custom actions which can be used to run
# custom Python code.
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/core/actions/#custom-actions/
# This is a simple example for a custom action which utters "Hello World!"



from typing import Any, Text, Dict, List
import pyodbc
import yaml
from datetime import date, timedelta, datetime
from dateutil.parser import parse
from random import random,randint
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction
from rasa_sdk.events import SlotSet
#from AmeXA.amexa.DisputeProcess import *
#from AmeXA.amexa.CommonUtilities import *


def ConnectSQLDB():
    config = {}
    with open("DBConfig.yml", "r") as stream:
        config = yaml.safe_load(stream)
    stream.close()
    server = config['server']
    database = config['database']
    username = config['username']
    password = config['password']

    print("Connecting to " + server + " " + "Database: " + database)

    # Connect to Database
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER=' + server + ';DATABASE=' + database + ';UID=' + username + ';PWD=' + password)
    cursor = cnxn.cursor()
    return cnxn, cursor   # Return the connection and cursor object

class ActionHelloWorld(Action):

    def name(self) -> Text:
        return "action_activate_card"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        card_num = tracker.get_slot("card4d")
        message = "The card number ending with {} is Activated and ready for use!".format(card_num)
        dispatcher.utter_message(text=message)
        return []


# ********* action_card_activation_form - Form Action for Card Activation  ***********
class ActionCardActivationForm(FormAction):

    def name(self) -> Text:
        return "action_card_activation_form"
    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        return ["name", "card4d", "pin"]

    def submit(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],
    ) -> List[Dict]:
        dispatcher.utter_message(text="Great, Good Job!")

        card4d = getLastSlotValue(tracker.get_slot("card4d"))
        pin = getLastSlotValue(tracker.get_slot("pin"))
        name = getLastSlotValue(tracker.get_slot("name"))
        return [SlotSet("card4d", card4d), SlotSet("pin", pin), SlotSet("name", name)]


# *********** payment_form - Form Action for Payments  **************
class ActionPaymentForm(FormAction):

    def name(self) -> Text:
        return "payment_form"
    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        payBalance = getLastSlotValue(tracker.get_slot("payBalance"))

        if payBalance is not None:
            if ("Total".lower() in payBalance.lower()) or ("Statement".lower() in payBalance.lower()) or \
                    ("all".lower() in payBalance.lower()) or ("full".lower() in payBalance.lower()) or ("last".lower() in payBalance.lower()):
                return ["payBalance", "payDate"]
            else:
                return ["payBalance", "payDate", "amount"]
        else:
            return ["payBalance", "payDate"]

    def submit(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],
    ) -> List[Dict]:
        dispatcher.utter_message(text="Thanks for providing all the details. Let me process these..")
        # Custom logic to update slot values for payBalance and payDate
        # Getting Balances from Database
        DBObjects = ConnectSQLDB()
        cursor = DBObjects[1]

        payBalance = getLastSlotValue(tracker.get_slot("payBalance"))
        payDate = getLastSlotValue(tracker.get_slot("payDate"))
        amount = getLastSlotValue(tracker.get_slot("amount"))
        card4d = getLastSlotValue(tracker.get_slot("card4d"))

        print("******** payment_form: Updating Payment Balance and Pay date Slot. Getting details from Database...")
        tsql = "select * from dbo.Customer where Card4d = " + str(card4d)
        cursor.execute(tsql)
        result = cursor.fetchone()

        #  Quantify the Pay Balance based on user's input
        if (amount is not None):
            payBalance = amount    # If custom payment amount is entered, update payment balance
        elif ("Total".lower() in payBalance.lower()) or ("All".lower() in payBalance.lower()) or ("full".lower() in payBalance.lower()):
            payBalance = result[9]  # If user said he/she wants to pay, Total Balance, set Total Balance pulled from DB record
        else:
            payBalance = result[8]  # If user said he/she wants to pay, Statement Balance, set Statement Balance pulled from DB record

        #  Update the Pay Date based on user's input

        if(payDate is not None and ("Today".lower() in payDate.lower() or "Current".lower() in payDate.lower())):
            payDate = str(datetime.now().strftime("%d-%m-%Y %H:%M:%S"))
        else:
            payDate = str((datetime.now() + timedelta(days=1)).strftime("%d-%m-%Y %H:%M:%S"))

        #cursor.close()
        return [SlotSet("payBalance", payBalance), SlotSet("payDate", payDate)]



# ************ action_payment -  Action for Payment ***************

class ActionPayment(Action):

    def name(self) -> Text:
        return "action_payment"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        card4d = getLastSlotValue(tracker.get_slot("card4d"))
        payBalance = getLastSlotValue(tracker.get_slot("payBalance"))
        payDate = getLastSlotValue(tracker.get_slot("payDate"))

        DBObjects = ConnectSQLDB()
        cursor = DBObjects[1]
        cnxn = DBObjects[0]
        print("******** action_payment: Query Customer table to get customer details. Calling Database ...")
        tsql = "select * from dbo.Customer where Card4d = " + str(card4d)
        cursor.execute(tsql)
        result = cursor.fetchone()
        payID = 'PMT'+ str(randint(10000, 99999))
        CustID = result[0]
        FirstName = result[1]
        LastName = result[2]
        AccountNum = result[3]

        print("******** action_payment: Query CustomerPaymentAccount table to get Bank details. Calling Database ...")
        tsql = "select * from dbo.CustomerPaymentAccounts where CustID = " + str(CustID) + " AND isDefault = 'Y' "
        cursor.execute(tsql)
        result = cursor.fetchone()
        BankName = result[3]
        BankAccount = result[4]

        print("******** action_payment: Update Payment table with collected details. Calling Database ...")
        tsql = "INSERT INTO dbo.Payments VALUES "\
            "("+ "\'" + str(payID) +"\'" + ", " + str(CustID) + ", " + "\'" + FirstName +"\'" + ", " + \
               "\'" + LastName + "\'" + ", " + str(AccountNum) + ", " + str(card4d) + ", " + "\'" + BankName +"\'" + ", " + \
            str(BankAccount) + ", " + str(payBalance) + ");"

        cursor.execute(tsql)
        cnxn.commit()

        #message = "Congratulations, Your Payment is successful on card number ending with {card4d}. \n It'll be reflected on your Account within 24 hours"
        dispatcher.utter_message(template="utter_payment_confirm_success")
        return []


# **************  authentication_form - Authenticate User form ***********

class UserAuthenticationForm(FormAction):

    def name(self) -> Text:
        return "authentication_form"
    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        return ["name", "card4d", "pin"]

    def submit(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],
    ) -> List[Dict]:
        print("**** Autheticating User from DB. CardNo: " +getLastSlotValue(tracker.get_slot("card4d")) + " PIN: "+ getLastSlotValue(tracker.get_slot("pin")))
        auth_flag = authenticateUser(getLastSlotValue(tracker.get_slot("card4d")), getLastSlotValue(tracker.get_slot("pin")))
        returnMsg=""
        print("Is User authenticated: " + str(auth_flag))
        if auth_flag == True:
            returnMsg = "Thanks {name}, Your Authentication Successful"
            return [SlotSet("isAuthenticated", True)]
        else:
            returnMsg = "Authentication Unsuccessful"
            return [SlotSet("isAuthenticated", False)]
        dispatcher.utter_message(text=returnMsg)



# ************  action_set_authentication - Sets slot - isAuthenticated, to verify if user is authenticated or not  ***************

class ActionSetSlotisAuthenticated(Action):

    def name(self) -> Text:
        return "action_set_authentication"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        auth_flag = authenticateUser(getLastSlotValue(tracker.get_slot("card4d")),  getLastSlotValue(tracker.get_slot("pin")))
        dispatcher.utter_message(text="User Autehtication: " + str(auth_flag))

        # Also use this function to pull Customer's balances (Statement Balance and Total Balance) and Set Slots

        DBObjects = ConnectSQLDB()
        cursor = DBObjects[1]
        # Select Query
        print(" ******* Connecting to DB - Getting Balances of the customer and setting slots ")

        card4d = getLastSlotValue(tracker.get_slot("card4d"))
        tsql = "select * from dbo.Customer where Card4d =" + str(card4d)
        cursor.execute(tsql)
        result = cursor.fetchone()
        stBal = result[8]
        totBal = result[9]
        name = getLastSlotValue(tracker.get_slot("name"))
        cursor.close()
        return [SlotSet("isAuthenticated", auth_flag), SlotSet("stBal", stBal), SlotSet("totBal", totBal), SlotSet("name", name)]


# ************  action_check_dispute - Check if any dispute case is already open on the card  ***************

class ActionCheckDispute(Action):

    def name(self) -> Text:
        return "action_check_dispute"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        #card_num = tracker.get_slot("card4d")
        returnedRows = CheckDisputeOnCard(getLastSlotValue(tracker.get_slot("card4d")))
        hasExistingDispute = ""
        if (len(returnedRows) > 0) :
            hasExistingDispute = "true"
            print("User has existing dispute on card: "+ hasExistingDispute)
            returnedRows = '\n'.join(returnedRows)
            #dispatcher.utter_message(text=returnedRows)
            dispatcher.utter_message(text="")
            return [SlotSet("hasExistingDispute", True), SlotSet("DisputeDetails", returnedRows)]
        else:
            hasExistingDispute = "false"
            print("User has existing dispute on card: " + hasExistingDispute)
            dispatcher.utter_message(text="")
            return [SlotSet("hasExistingDispute", False)]



# ********** action_show_transactions - Show user's recent transactions ***********

class ActionDisplayTransactions(Action):

    def name(self) -> Text:
        return "action_show_transactions"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        #card_num = tracker.get_slot("card4d")
        returnedRows = GetTransactionsOnCard(getLastSlotValue(tracker.get_slot("card4d")))
        dispatcher.utter_message(text=returnedRows)
        return []


# ********** action_deDupe_slots - Remove duplicate values from the slots ***********

class ActionDisplayTransactions(Action):

    def name(self) -> Text:
        return "action_deDupe_slots"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        card4d = tracker.get_slot("card4d")
        pin = tracker.get_slot("pin")
        amount = tracker.get_slot("amount")
        dispReason = tracker.get_slot("dispReason")
        name = tracker.get_slot("name")
        TransID = tracker.get_slot("TransID")
        if (not (isinstance(card4d, str)) and card4d is not None and len(card4d) > 1):
            card4d = card4d[0]
        if (not (isinstance(pin, str)) and pin is not None and len(pin) > 1):
            pin = pin[0]
        if (not (isinstance(amount, str)) and amount is not None and len(amount) > 1):
            amount = amount[0]
        if (not (isinstance(dispReason, str)) and dispReason is not None and len(dispReason) > 1):
            dispReason = dispReason[0]
        if (not (isinstance(name, str)) and name is not None and len(name) > 1):
            name = name[0]
        if (not (isinstance(TransID, str)) and TransID is not None and len(TransID) > 1):
            TransID = TransID[0]
        dispatcher.utter_message(text="")
        return [SlotSet("card4d", card4d), SlotSet("pin", pin), SlotSet("amount", amount),
                SlotSet("dispReason", dispReason), SlotSet("name", name), SlotSet("TransID", TransID)]

# **************  dispute_form - Dispute form ***********

class DisputeForm(FormAction):

    def name(self) -> Text:
        return "dispute_form"
    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        return ["TransID", "amount", "dispReason"]

    def submit(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],
    ) -> List[Dict]:
        dispatcher.utter_message(text="")
        return []

# *********** action_create_dispute_case - Create a dispute case with collected Information ******

class ActionDisplayTransactions(Action):

    def name(self) -> Text:
        return "action_create_dispute_case"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        #card_num = tracker.get_slot("card4d")

        CreateDisputeCase(getLastSlotValue(tracker.get_slot("card4d")), getLastSlotValue(tracker.get_slot("TransID")),
                          getLastSlotValue(tracker.get_slot("dispReason")), getLastSlotValue(tracker.get_slot("amount")))
        dispatcher.utter_message(text="")
        return []

def authenticateUser(card4d, pin):
    DBObjects = ConnectSQLDB()
    cursor = DBObjects[1]
    # Select Query
    print('Authenticating user. Pulling DB record')
    #print(isinstance(card4d, str))
    if(not(isinstance(card4d, str)) and card4d is not None and len(card4d) > 1):
        card4d = card4d[0]
    if (not(isinstance(pin, str)) and pin is not None and len(pin) > 1):
        pin = pin[0]
    tsql = "select * from dbo.Customer where Card4d = "+ str(card4d) + " AND PIN = " + str(pin)
    #print(tsql)
    cursor.execute(tsql)
    result = cursor.fetchall()
    #print(tsql)
    #print(result)
    count = len(result)

    if (count > 0):
        return True
    else:
        return False

#print(authenticateUser("3456", "123456"))   # Only for testing



def CheckDisputeOnCard(card4d):
    DBObjects = ConnectSQLDB()
    cursor = DBObjects[1]
    # Select Query
    print('Connecting to DB - Checking existing dispute on Card')

    if (not(isinstance(card4d, str)) and card4d is not None and len(card4d) > 1):
        card4d = card4d[0]
    tsql = "select * from dbo.Customer where Card4d ="+ str(card4d)
    cursor.execute(tsql)
    result = cursor.fetchone()
    cardNumber = result[3]
    tsql = "select top(2) * from dbo.Dispute where cardNumber = "+ str(cardNumber) + " AND Status = 'Pending' "
    print(tsql)
    cursor.execute(tsql)
    result = cursor.fetchall()
    count = len(result)
    returnStr = []

    if count > 0:
        for row in result:
            # print(str(row))
            returnStr.append("Dispute ID: "+str(row[0]) + " | Date: " + str(row[3]) + " | Merchant: " + str(row[6]) +
                 " | Amount: " + str(row[13]) + " | Reason: " + str(row[9]))
    #cursor.close()
    return returnStr

#print(CheckDisputeOnCard("3456"))

def GetTransactionsOnCard(card4d):
    DBObjects = ConnectSQLDB()
    cursor = DBObjects[1]
    # Select Query
    print('Connecting to DB - Query last 2 months transactions')
    last60days = date.today() - timedelta(days = 30)
    #print(last60days)
    if (not(isinstance(card4d, str)) and card4d is not None and len(card4d) > 1):
        card4d = card4d[0]
    tsql = "select * from dbo.Customer where Card4d =" + str(card4d)
    cursor.execute(tsql)
    result = cursor.fetchone()
    cardNumber = result[3]
    tsql = "select * from dbo.Transactions where CardNumber = " + str(cardNumber) + " and Trans_Date > " + "'" + str(last60days) + "'"
    #print(tsql)
    cursor.execute(tsql)
    result = cursor.fetchall()
    count = len(result)
    returnStr = []

    if count > 0:
        for row in result:
            #print(str(row))
            returnStr.append("TransID: " + str(row[0]) + " | Date: " + str(row[2]) + " | Merchant: " + str(row[6]) +
                " | Amt: " + str(row[10]) + " | Desc: " + str(row[4]) + " | Card: " + str(row[11]))
    #cursor.close()
    return '\n'.join(returnStr)

#print(GetTransactionsOnCard("3559"))


# ********    This function is a mapping for Reason Code   *************
def getReasonCode(reason):
  reasonCode = "DC000"
  if "duplicate".lower() in reason.lower() or "twice".lower() in reason.lower():
      reasonCode = "DT480"
  elif "Incorrect".lower() in reason.lower() or "wrong".lower() in reason.lower():
      reasonCode = "DI580"
  elif "not delivered".lower() in reason.lower():
      reasonCode = "DN680"
  elif "Quality".lower() in reason.lower() :
      reasonCode = "DQ780"
  elif "Defective".lower() in reason.lower() or "broken".lower() in reason.lower() :
      reasonCode = "DD880"
  elif "billed twice".lower() in reason.lower() :
      reasonCode = "DB980"
  else:
      reasonCode = "D0180"

  return reasonCode




def CreateDisputeCase(card4d, TransID, dispReason, amount):
    DBObjects = ConnectSQLDB()
    cursor = DBObjects[1]
    cnxn = DBObjects[0]

    if (not(isinstance(card4d, str)) and card4d is not None and len(card4d) > 1):
        card4d = card4d[0]
    if (not(isinstance(TransID, str)) and TransID is not None and len(TransID) > 1):
        TransID = TransID[0]
    if (not(isinstance(dispReason, str)) and dispReason is not None and len(dispReason) > 1):
        dispReason = dispReason[0]
    if (not(isinstance(amount, str)) and amount is not None and len(amount) > 1):
        amount = amount[0]

    # Query Transaction Table to fetch details
    print("**** Creating Dispute Case for Card: " + str(card4d) + " Transaction ID: "+ str(TransID))
    tsql = "select * from dbo.Transactions where TransID = " + str(TransID)
    print(tsql)
    cursor.execute(tsql)
    result = cursor.fetchone()
    count = len(result)
    merchantName = result[6]
    merchantID = result[5]
    merchantAdds = result[7]
    cardNumber = str(result[11])
    CustID = str(result[1])
    reasonCode = getReasonCode(str(dispReason))
    disputeID = "D-"+ TransID + str(randint(100, 999))
    tsql = "INSERT INTO dbo.Dispute VALUES " \
           "(" + "\'" + disputeID + "\'" + ", " + str(
        TransID) + ", " + CustID + "," + "CURRENT_TIMESTAMP" + ", " + "\' \' " + "," + \
           "\'" + merchantID + "\'" + ", " + "\'" + merchantName + "\'" + ", " + "\'" + merchantAdds + "\'" + ", " + \
           "\'" + reasonCode + "\'" + ", " + "\'" + dispReason + "\'" + ", " + \
           "\'" + "Pending" + "\'" + ", " + cardNumber + ", " + "\'" + "New Dispute Case" + "\'" + ", " + str(
        amount) + ");"
    print(tsql)
    count1 = cursor.execute(tsql)
    cnxn.commit()
    #cursor.close()

#CreateDisputeCase("3456", "10011", "Duplicate", "444")
#print('\n'.join(GetTransactionsOnCard("670000000123559")))



# This returns the slots value and sends the last value set. Remember Slots can contain multiple values in list.
def getLastSlotValue(slotName):
    if (slotName is not None and (isinstance(slotName, list) or isinstance(slotName, tuple) or isinstance(slotName, dict)) and len(slotName) > 1):
        last = len(slotName)
        slotName = slotName[last-1]
    return slotName


