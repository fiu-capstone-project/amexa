CREATE TABLE dbo.Payments (
    PaymentID varchar(255),
	CustID int,
	FirstName varchar(255),
    LastName varchar(255),
    AccountNumber bigint,
	Card4d int,	
	PayBankName varchar(255),
	PayBankAccount bigint,
	Pay_amount decimal(16,2)
 );
 
INSERT INTO dbo.Payments
VALUES (
'PMT11011111',
1001, 
'Abhishek', 
'Kumar', 
670000000123456,
3456,
'CHASE Bank',
4800098762453614,
700.00
);