CREATE TABLE dbo.CustomerPaymentAccounts (
   	CustID int,
	FirstName varchar(255),
    LastName varchar(255),
    PayBankName varchar(255),
	PayBankAccount bigint,
	PayBankRoutingNumber bigint,
	isDefault varchar(8)
 );
 
 
INSERT INTO dbo.CustomerPaymentAccounts
VALUES (
1001, 
'Abhishek', 
'Kumar', 
'CHASE Bank',
4800098762453614,
567654167,
'Y'
);

INSERT INTO dbo.CustomerPaymentAccounts
VALUES (
1001, 
'Abhishek', 
'Kumar', 
'Bank of America',
7700098762457834,
873526273,
''
);

INSERT INTO dbo.CustomerPaymentAccounts
VALUES (
1004, 
'James', 
'DaVinci', 
'CHASE Bank',
3627098762459983,
567654167,
''
);

INSERT INTO dbo.CustomerPaymentAccounts
VALUES (
1004, 
'James', 
'DaVinci', 
'Wells Fargo',
8966098762456352,
874526725,
'Y'
);

INSERT INTO dbo.CustomerPaymentAccounts
VALUES (
1005, 
'Bianca', 
'Cruz', 
'CHASE Bank',
4800098762459987,
567654167,
'Y'
);

INSERT INTO dbo.CustomerPaymentAccounts
VALUES (
1005, 
'Bianca', 
'Cruz', 
'Bank of America',
7700098762488725,
873526273,
''
);