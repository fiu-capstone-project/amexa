
from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction
from rasa_sdk.events import SlotSet
from datetime import date, timedelta, datetime
from random import random,randint
from AmeXA.amexa.ConnectDB import *


import pyodbc


def CheckDisputeOnCard(card4d):

    cursor = ConnectSQLDB()
    # Select Query
    print('Reading data from table')
    tsql = "select * from dbo.Dispute where CardNumber ="+card4d + " AND Status = 'Pending' "
    cursor.execute(tsql)
    result = cursor.fetchall()
    count = len(result)
    returnStr = []

    if count > 0:
        for row in result:
            # print(str(row))
            returnStr.append("Dispute ID: "+str(row[0]) + " | Received Date: " + str(row[3]) + " | Merchant Name: " + str(row[6]) +
                 " | Amount: " + str(row[13]) + " | Reason: " + str(row[9]) + " | Card Number: " + str(row[11]))

    return returnStr


def GetTransactionsOnCard(CardNumber):
    cursor = ConnectSQLDB()
    # Select Query
    print('Reading data from table')
    last60days = date.today() - timedelta(days = 30)
    #print(last60days)
    tsql = "select * from dbo.Transactions where CardNumber = " + CardNumber + " and Trans_Date > " + "'" + str(last60days) + "'"
    #print(tsql)
    cursor.execute(tsql)
    result = cursor.fetchall()
    count = len(result)
    returnStr = []

    if count > 0:
        for row in result:
            #print(str(row))
            returnStr.append("Transaction ID: " + str(row[0]) + " | Transaction Date: " + str(row[2]) + " | Merchant Name: " + str(row[6]) +
                " | Amount: " + str(row[10]) + " | Description: " + str(row[4]) + " | Card Number: " + str(row[11]))

    return returnStr


def CreateDisputeCase(card4d, TransID, dispReason, amount):
    DBObjects = ConnectSQLDB()
    cursor = DBObjects[1]
    cnxn = DBObjects[0]
    # Select Query
    print("Creating Dispute Case for Card: " + card4d + " Transaction ID: "+ TransID)
    tsql = "select * from dbo.Transactions where TransID = " + TransID
    #print(tsql)
    cursor.execute(tsql)
    result = cursor.fetchone()
    count = len(result)
    merchantName = result[6]
    merchantID = result[5]
    merchantAdds = result[7]
    reasonCode = "DT880"
    receivedDate = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    disputeID = "D-"+ TransID + str(randint(100, 999))
    tsql = "INSERT INTO dbo.Dispute VALUES " \
           "("+ "\'" + disputeID +"\'" + ", " + TransID + ", 1004 ," +  "\'" + receivedDate + "\'" + ", " + "\' \' " + "," + \
           "\'" + merchantID + "\'" + ", " + "\'" + merchantName + "\'" + ", " + "\'" + merchantAdds + "\'" + ", " + \
           "\'" + reasonCode + "\'" + ", " + "\'" + dispReason + "\'" + ", "  + \
           "\'" + "Pending" + "\'" + ", " + "670000000123559" + ", " + "\'" + "Testing Dispute Comment" + "\'" + ", " + str(480) + ");"

    print(tsql)
    count1 = cursor.execute(tsql)
    print(cnxn.commit())

#CreateDisputeCase("3456", "10011", "Test", "567")
#print('\n'.join(GetTransactionsOnCard("670000000123559")))