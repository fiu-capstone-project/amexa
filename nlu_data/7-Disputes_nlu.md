## intent:init_dispute
- I want to [dispute](case) my transaction
- I want to file a [dispute](case) case on my transaction
- I want to claim a [dispute](case) case
- I want to file [dispute](case) on my card
- file [dispute](case) on my card
- create a [dispute](case) case
- [dispute](case) a transaction
- I have a [problem](case) on one of the transaction
- file a [case](case) on one transaction
- merchant billed me [incorrectly](case)
- [duplicate billing](case) by merchant
- product is [defective](case)
- [incorrect](case) billing
- [dispute](case) billing
- [dispute](case) merchant

## intent:dispute_case_entry
- [first](disputeRecord) transaction
- [last](disputeRecord) transaction
- [2nd](disputeRecord) transaction
- [second](disputeRecord) transaction
- [third](disputeRecord) transaction
- [fourth](disputeRecord) transaction
- [Second](disputeRecord) transaction
- [First](disputeRecord) transaction
- [Last](disputeRecord) transaction
- transaction with ID [10011](TransID)
- Transaction with transaction id [10042](TransID)
- Transaction id [10044](TransID)
- Transaction with merchant [Publix](merchant)
- Transaction with merchant id [PB12345](mercahntID)
- transaction with [$640.30](amount)
- The one with dollar [280](amount)
- the one with amount [145.50](amount)
- The one with transaction id [10054](TransID)

## intent:transaction_id_entry
- transaction id [10061](TransID)
- Transaction ID is [10042](TransID)
- [10052](TransID)
- ID [10051]()
- TransID: [10053](TransID)
- trans id [10061](TransID)
- id [10054](TransID)

## intent:full_partial_dispute
- [partial](disputeFraction)
- [full](disputeFraction) amount
- [partial](disputeFraction) amount
- [Full](disputeFraction) amount
- [Partial](disputeFraction) amount
- I opt for [full](disputeFraction) dispute
- I opt for [full](disputeFraction) amount
- Make it [partial](disputeFraction) amount
- make it [full](disputeFraction) amount
- Keep it [Full](disputeFraction) dispute amount
- keep it [partial](disputeFraction) dispute amount

## intent:amount_entry
- [$140.00](amount)
- $[1678.50](amount)
- amount [1245.00](amount)
- dispute amount [$450](amount)
- ok, the dispute amount would be [230.00](amount)  

## intent:confirm_dispute_intent
- yes I want to dispute a transaction
- Yes disputes
- yes dispute
- dispute
- Exactly, I want to file dispute
- file dispute
- dispute a transaction
- Yes, I want to file for dispute
- Correct, file a dispute 
- Right, dispute transaction
- Yes, file a Dispute
- That's correct, dispute
- That's true, I have a dispute on transaction
- Right, file a dispute
- Right, Dispute
- Right, Dispute a Transaction
- That's ok, I want to continue with a new dispute case

## intent:no_dispute_continue
- No I don't want to continue with this dispute process
- No I quit
- Stop the dispute process
- I'll not create a new dispute
- Stop this dispute
- I don't want to continue
- I don't want to create any dispute
- No dispute 
- I don't want to file any dispute 
- Please stop this case
- I think I am good, don't want to continue with this
- Oh ok. The dispute is already there, I wish not to continue
- I am sorry, I don't want to go further in this case
- I am sorry, I don't want to dispute anything
- I think the dispute case is there, I don't want to duplicate 

## intent:dispReason_entry
- [Duplicate billing](dispReason)
- The [Merchant billed twice](dispReason)
- dispute reason [Goods and Services not delivered](dispReason)
- [Goods and services not delivered](dispReason)
- [Defective Product](dispReason)
- [Incorrect Amount Billed](dispReason)
- [Quality issue - Product and Services](dispReason)
- The merchant did [Duplicate Billing](dispReason) on my card

## regex:TransID
- (?<!\d)(?!0000)\d{5}(?!\d)
