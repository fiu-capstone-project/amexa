## intent:init_card_activation
- can I activate my card?
- I need to activate my new card
- activate card
- get my card activated
- make my card active
- want to make my card working
- start my new card 
- I am not able to use my card

## intent:PIN_entry
- Pin is [267966](pin)
- my pin is [287634](pin)
- 4 digit [098734](pin) 
- pin for my card is [275327](pin)
- my card in is as follows [886289](pin)
- note down my card [298527](pin)
- new card [283727](pin)
- the pin [297689](pin)
- [123456](pin)

## regex:pin
- (?<!\d)(?!000000)\d{6}(?!\d)

## regex:card4d
- (?<!\d)(?!0000)\d{4}(?!\d)

## intent:card4d_entry
- My card number is [6678](card4d)
- card number is [2297](card4d)
- Please note my card number [9735](card4d)
- [3456](card4d)

## intent:confirm_card_activation_intent
- I want to activate my card
- That's right, activate my card
