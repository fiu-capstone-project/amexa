## intent:affirm
- yes
- indeed
- of course
- that sounds good
- correct
- exactly
- thats true
- makes sense
- I am with you
- perfect
- that makes perfect sense
- Correct
- You are absolutely correct
- You bet
- Yes I agree
- You are right
- Right
- That's true
- Of course
- Yes, you are correct
- Accept
- Awesome!
- Cool
- Good
- Great
- I accept
- I accept.
- I agree
- I am using it
- I changed my mind. I want to accept it
- I do
- I get it
- I guess so
- I have used it in the past
- I will
- I'd absolutely love that
- I'm sure I will!
- I'm using it
- Nice
- OK
- Ofcourse
- Oh yes
- Oh, ok
- Ok
- Ok let's start
- Ok.
- Okay
- Okay!
- PLEASE
- SURE
- Sure
- Sweet
- That would be great
- YES
- YUP
- Yea
- Yeah
- Yeah sure
- Yep
- Yep that's fine
- Yep!
- Yepp
- Yes I do
- Yes please
- Yes please!
- Yes, I accept
- Yes.
- Yup
- a little
- absolutely
- accept
- accepted
- agreed
- agree
- ah ok
- alright
- alright, cool
- amayzing
- amazing!
- awesome
- awesome!
- confirm
- cool
- cool :)
- cool story bro
- cool!
- coolio
- definitely yes without a doubt
- done
- fair enough
- fcourse
- fine
- fuck yeah!
- go
- go ahead
- go for it
- going super well
- good.
- great
- great lets do that
- great!
- hell yeah
- hell yes
- hm, i'd like that
- how nice!
- i accept
- i agree
- i am!
- i want that
- i will!
- it is ok
- its okay
- ja
- ja cool
- ja thats great
- jezz
- jo
- k
- kk
- lets do it
- lets do this
- not bad
- ofcoure i do
- ofcourse
- oh awesome!
- oh cool
- oh good !!
- oh super
- ok
- ok cool
- ok fine
- ok friend
- ok good
- ok great
- ok i accept
- ok amexa
- ok, I behave now
- ok, I understood
- ok, Amexa
- ok...
- okay
- okay cool
- okay sure
- okay..
- oki doki
- okie
- ook
- oui
- please
- si
- sort of
- sure
- sure thing
- sure!
- that is cool
- that ok
- that sounds fine
- that's great
- thats fine
- thats good
- thats great
- very much
- well yes
- ya
- ya cool
- ya go for it
- ya i want
- ya please
- ya thats cool
- ye splease
- yea
- yeah
- yeah do that
- yeah sure
- yeah'=
- yeah, why not
- yeeeeezzzzz
- yeeees
- yep
- yep i want that
- yep if i have to
- yep please
- yep that's nice
- yep thats cool
- yep, will do thank you
- yep. :/
- yes ...
- yes I do
- yes accept please
- yes baby
- yes cool
- yes give me information
- yes go ahead
- yes go for it
- yes great
- yes i accept
- yes i agree
- yes i have built a bot before
- yes i have!
- yes it is
- yes it was okay
- yes of course
- yes pleae
- yes please
- yes please!
- yes pls
- yes sir
- yes that's great
- yes that's what i want
- yes you can
- yes'
- yes, I'd love to
- yes, cool
- yes, give me information, please
- yes,i am
- yes.
- yesh
- yess
- yessoo
- yesss
- yesssss
- yesyestyes
- yesyesyes
- yez
- yop
- you asked me a yes or no question, which i answered with yes
- you got me, I accept, if you want me to
- yres
- ys
- yup
- yyeeeh
- Okay cool
- ok..
- considering
- More a less
- cool beans
- sounds good!
- really
- Yes that's correct
- you understood me
- affirmative

## intent:deny
- no
- never
- I don't think so
- don't like that
- no way
- not really
- that's not correct
- that's not right
- it's wrong
- wait, it's incorrect
- that's wrong
- that's incorrect
- Well no
- well no
- Well that's not correct
- I don't want to
- I don't want to give it to you
- I don't want to say
- I dont want to tell
- I'm not giving you my email address
- I'm not going to give it to you
- NEIN
- NO
- NO DON"T WANT THIS!
- Nah
- Neither
- Never
- Nevermind
- No
- No thank you
- No, not really.
- No, thank you
- No.
- Nopes
- Not really
- absolutely not
- decline
- definitely not
- deny
- i decline
- i don not like this
- i don't want either of those
- i don't want to
- i don't want to give you my email
- i dont want to
- i dont want to accept :P lol
- i guess it means - no
- i'm afraid not
- i'm not sure
- it is going pretty badly
- it sucks
- it sux
- n
- na
- nah
- nah I'm good
- nah not for me
- nah, first time
- nah, i'm good
- nehi
- nein
- neither
- never mind
- no :(
- no I dont want
- no I haven't decided yet if I want to sign up
- no and no again
- no bots at all
- no go
- no i can't
- no i don't accept
- no i dont want to
- no i dont want to accept :P lol
- no i won't
- no ma'am
- no sir
- no sorry
- no thank s
- no thank you
- no thanks
- no you did it wrong
- no!!!!
- no, i hate it
- no, my frst time
- no, thank you
- no, thanks
- no, thankyou
- no. u r idiot
- non
- noooooooooo
- noooooooooooooooooooooooooooooooooooooooo
- nop
- nope
- nope!
- nope. i am good
- not going well at all
- not right now
- not yet
- nö
- sorry not right now
- still dont want to tell
- thanks but no thanks
- this sucks
- very bad
- I do not need help installing
- I don't wanna tell the name of my company
- no stop
- stop it, i do not care!!!
- none of them
- I don't agree

## intent:customer_satisfied
- No I am good. Thank you for your support
- No I am good. Thank you
- I am good so far
- Thank you. I have nothing else
- Great! Thank you
- That's it. Thank you
- That's what I was looking for
- Cool. Thank you. I am good
- That's all I wanted
- I am satisfied
- I am good
- I am good for now. Thank you
- That's all I was looking for, thanks for your support
- Thannks for your support, that's all I wanted
- I got what I was looking for, thank you
- I am good. Thanks for your help
- No further help
- No that's all
- So far so good
- Thank you for your help and support
- This is all I was looking for
- I got my work done
- That's all I wanted
- I am done
- I am done, thank you

## intent:bot_challenge
- are you a bot?
- are you a human?
- am I talking to a bot?
- am I talking to a human?
- are you real?

## intent:customer_not_understand
- No I am good. Thank you for your support
- I am good so far
- Thank you. I have nothing else
- Great! Thank you
- That's it. Thank you
- That's what I was looking for
- Cool. Thank you. I am good

## intent:name_entry
- My name is [Abhishek](name)
- Name [Arjun](name)
- Note my name as [Robin](name)
- [Bianca](name)
- Find my name as [Priyanka](name)
- I am [Manuel](name)

## intent:date_entry
- date is [02/08/2020](date)
- Date [07/06/2019](date)
- Payment Date [09/12/2021](date)
- Due date [31/07/2020](date)
- The date is [26/01/2017](date)
- Keep the date as [16/02/2019](date)
- Ok. Date is [11/05/2022](date)
- ok, lets keep the date as [10/06/2019](date)
- Schedule it on [06/11/2020](date)
- schedule the payment on [09/07/1967](date)
- [11/24/2020](date)
- its [03/14/2020](date)

## intent:confirm_correct_info
- Yes these information is correct
- Yes please go ahead and make the change
- I approve the changes
- Submit the information
- Please proceed with these information
- Yes correct, please go ahead and submit
- Right information
- Please go next
- Please submit
- Yes Submit
- Submit
- I confirm, please proceed
- Yes please proceed
- I approve. Please proceed
- proceed please
- submit please
- hit submit
- approved
- submit the transaction
- yes proceed
- Please proceed
- please go ahead
- go ahead please
- go ahead
- I confirm, go ahead
- get going
- move ahead
- done, submit
- done proceed
- done, please submit
- please go next
- take action
- I confirm, take action
- I approve please take action
- I approve the details
- I confirm the details
- Go Ahead!

## intent:out_of_scope
- that's not what I want to do
- wait stop
- you're no help
- this is no help at all
- how old are you
- I want to order a pizza
- tell me the weather
- this isn't working
- I already told you that
- don't like that
- I don't want to tell you that
- none of your business
- that's not right
- stop asking
- nevermind
- I want to do something else
- I changed my mind

## intent:human_handoff
- Can I speak to anyone who can really help me?
- Could I talk to [Tyrone King](name)?
- I don't wanna talk to a bot
- I dont like to talk to a machine
- I want to talk to a human
- I want to talk to the founders
- are there also humans working for your company?
- can I speak to a person?
- can i please speak to a human?
- can you forward me to your team
- can you please connect me to a real rasa employee?
- can you put me in touch with a human?
- do you have human support ?
- gimme a proper human
- give me a human
- give me a human now
- human handoff
- i dont wanna talk to a bot
- i want to speak to a [manager](job_function)
- i want to speak to a real person
- i want to speak to customer service
- i want to talk to a human
- i want to talk to a person
- i want to talk to human
- i want to talk to someone at rasa
- i want to talk to someone else
- i want to talk to someone who is smarter than you
- i would like to speak to a person
- i'd rather speak with a real rasa employee
- id like to talk to a real rasa employee
- let me speak with a real person please
- let me talk to a human
- let me talk to a real person
- please give me a human
- service agent
- someone from customer care
- speak to a real person
- talking to a bot is stupid
- that's annoying I'd like to speak to someone real
- thats not helping, can i talk to human?
- wrong i want to speak to a human
- can i speak to human
- can i speak to your human
- i want to chat with human
- How do I talk to a human
- talk with a human
- Can i talk to a human instead
- nevermind.... you're not human ... I need to talk to a live person
- Can you get a human to assist me?
- Can i talk to a human?
- Can I talk to a human
- Can I speak to a human?
- can i speak to a human
- no, i want to talk to human
- can you hand a conversation over to a human?
- can I talk to human?
- can I talk to human
- talk to human
- i want human :(
- can i talk to human
- i want to talk to a human \
- i want to speak to human
- can i talk to a real person?
- connect me to a real person
- I need a real person
- can i took with a real person
- let me speak to a real person
- let me speak to a real person please
- i want to talk to a real person
